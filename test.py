import solid
from volume         import *
from transformation import *
from vtkviewer      import *
from gdml           import *
from material       import *

import numpy as _np
import os.path as _path
import matplotlib.pyplot as _plt
from mpl_toolkits.mplot3d import Axes3D

import math as _math

def solids() :

    # create world volume
    world_solid   = example_solid("world")
    world_volume  = Volume(None, [0,0,0],world_solid, "world_volume",None, 0, False, "G4_Galactic")

    # create box solid/volume
    box_solid       = example_solid("Box")
    box_volume      = Volume([0,0,0], [-750,-750,0],box_solid,"box_volume",world_volume, 0, False, "G4_Galactic")

    tubs_solid      = example_solid("Tubs")
    tubs_volume     = Volume([0,0,0], [-750,-550,0],tubs_solid,"tubs_volume",world_volume, 0, False, "G4_Galactic")

    cuttubs_solid   = example_solid("CutTubs")
    cuttubs_volume  = Volume([0,0,0], [-750,-350,0],cuttubs_solid,"cuttubs_volume",world_volume,0,False,"G4_Galactic")

    cons_solid      = example_solid("cons")
    cons_volume     = Volume([0,0,0], [-750,-150,0],cons_solid,"cons_volume",world_volume,0,False,"G4_Galactic")

    para_solid      = example_solid("Para")
    para_volume     = Volume([0,0,0], [-750,50,0],para_solid,"para_volume",world_volume,0,False,"G4_Galactic")

    trd_solid       = example_solid("Trd")
    trd_volume      = Volume([0,0,0], [-750,250,0],trd_solid,"trd_volume",world_volume,0,False,"G4_Galactic")

    #No mesh yet
    #trap_solid      = example_solid("Trap")
    #trap_volume     = Volume([0,0,0], [-750,450,0],trap_solid,"trap_volume",world_volume, 0, False, "G4_Galactic")

    sphere_solid    = example_solid("Sphere")
    sphere_volume   = Volume([0,0,0], [-750,650,0],sphere_solid,"sphere_volume",world_volume,0,False,"G4_Galactic")

    orb_solid       = example_solid("Orb")
    orb_volume      = Volume([0,0,0], [-750,850,0],orb_solid,"orb_volume",world_volume,0,False,"G4_Galactic")

    torus_solid     = example_solid("Torus")
    torus_volume    = Volume([0,0,0], [-750,1050,0],torus_solid,"torus_volume",world_volume,0,False,"G4_Galactic")

    pcon_solid      = example_solid("Polycone")
    pcon_volume     = Volume([0,0,0], [-750,1300,0],pcon_solid,"polycone_volume",world_volume,0,False,"G4_Galactic")

    #pgon_solid      = example_solid("Polyhedra")
    #pgon_volume     = Volume([0,0,0], [-750,1450,0],pgon_solid,"polyhedra_volume",world_volume,0,False,"G4_Galactic")

    eltube_solid = example_solid("EllipticalTube")
    eltube_volume= Volume([0,0,0], [-750,1650,0],eltube_solid,"elipticaltube_volume",world_volume,0,False,"G4_Galactic")

    ellipsoid_solid = example_solid("Ellipsoid")
    ellipsoid_volume= Volume([0,0,0], [-750,1850,0],ellipsoid_solid,"ellipsoid_volume",world_volume,0,False,"G4_Galactic")

    elcone_solid     = example_solid("EllipticalCone")
    elcone_volume    = Volume([0,0,0], [-750,2050,0],elcone_solid,"elcone_volume",world_volume,0,False,"G4_Galactic")

    paraboloid_solid = example_solid("Paraboloid")
    paraboloid_volume= Volume([0,0,0], [-750,2250,0],paraboloid_solid,"paraboloid_volume",world_volume,0,False,"G4_Galactic")

    hype_solid       = example_solid("Hype")
    hype_volume      = Volume([0,0,0], [-750,2450,0],hype_solid,"hype_volume",world_volume,0,False,"G4_Galactic")

    #tet_solid        = example_solid("Tet")
    #tet_volume       = Volume([0,0,0], [-750,2650,0],tet_solid,"tet_volume",world_volume,0,False,"G4_Galactic")

    #Faulty meshing
    #twistbox_solid   = example_solid("TwistedBox")
    #twistbox_volume  = Volume([0,0,0], [-750,2850,0],twistbox_solid,"twistbox_volume",world_volume,0,False,"G4_Galactic")

    polygon_pts      = [[-30,-30], [-30,30], [30,30], [30,-30], [15,-30], [15,15], [-15,15], [-15,-30]]
    zslices          = [[-60,[0,30],0.8], [-15,[0,-30],1.], [10,[0,0],0.6], [60,[0,30],1.2]]
    extruded_solid   = example_solid("ExtrudedSolid")
    extruded_volume  = Volume([0,0,0], [-750,3050,0],extruded_solid,"extruded_volume",world_volume,0,False,"G4_Galactic")

    # clip the world volume
    world_volume.setClip()
    m = world_volume.pycsgmesh()

    g = Gdml()
    g.add(world_volume)

    v1 = VtkViewer()
    v1.addSource(m)
    v1.view()

    return world_volume

def solid_individual(solid_type="box", translation=[0,0,0],
                     rotation=[0,0,0], add3DScatterPlot=False, output_name=None):
    """
    Allows for testing the meshing and placement of each primitive
    solid individually. The solid can be placed with any combination
    of translation and rotation.

    Inputs:
      solid:            string, type of solid
      translated:       list,  translation to placement.  By default,
                        no translation
      rotated:          list, Tait-bryan x-y-z angles of rotation (passive)
      add3DScatterPlot: boolean, prodces an additional 3d scatter plot
                        of mesh vertices if active
    """

    world_solid   = solid.Box("world_solid", 100000, 100000, 100000)
    world_volume  = Volume(None, [0,0,0], world_solid, "world_volume",
                           None, 0, False, "G4_Galactic")
    the_solid = example_solid(solid_type)
    vvolume  = Volume(rotation, translation, the_solid,
                      solid_type+"_volume", world_volume,
                      0, False, "G4_Galactic")

    world_volume.setClip()
    m = world_volume.pycsgmesh()

    if add3DScatterPlot:
        fig = _plt.figure()
        ax = fig.add_subplot(111, projection='3d')

        # mesh from solid only for comparison
        verts0 = (the_solid.pycsgmesh()).toVerticesAndPolygons()[0]
        # mesh from placed volume, should be same as above
        verts = m[1][0].toVerticesAndPolygons()[0]

        xs = [vert[0] for vert in verts]
        ys = [vert[1] for vert in verts]
        zs = [vert[2] for vert in verts]
        ax.scatter(xs, ys, zs, c='b', marker='o')

        ax.set_xlabel('X')
        ax.set_ylabel('Y')
        ax.set_zlabel('Z')
        ax.set_aspect('equal')

        _plt.show()

    if output_name is None:
        output_name= "individual_" + solid_type

    g = Gdml()
    g.add(world_volume)
    g.write(_output_path(output_name))

    v1 = VtkViewer()
    v1.addSource(m)
    v1.view()

    # gmad output
    _write_test_gmad(output_name)
    return world_volume

def vector_rotation_boolean(solid_type="rotation_test",
                            face=[0, 0, 1],
                            vector=[1, 1, 1]):
    """
    Test/recipe for starting with two vectors and rotating the volume
    by appropriate amount as given by the vectors.  Useful for
    getting a given face pointing in the correct direction.

    Use active for booleans, because it's active in GDML.
    """
    matrix = matrix_from(face, vector)
    # Get the matrix as angles.  All matrix functions in
    # transformation.py are right handed for consistency.  The below
    # function will therefore returnt he correct angles.
    angles = matrix2tbxyz(matrix)
    boolean(solid_type=solid_type,
            rotation=angles)

def vector_rotation_volume(solid_type="rotation_test",
                           face=[0, 0, 1],
                           vector=[1, 1, 1]):
    """
    Test/recipe for starting with two vectors and rotating the volume
    by appropriate amount as given by the vectors.  Useful for
    ensuring a given face points in the right direction.

    Use passive for volumes, because it's passive in GDML.
    """

    matrix = matrix_from(face, vector)
    # All matrix functions are in terms of active rotations..  so here
    # we reverse the rotation as volume rots are passive:
    angles = reverse(matrix2tbxyz(matrix))
    solid_individual(solid_type=solid_type,
                     rotation=angles,
                     output_name="vector_rot")

def boolean(solid_type="orb",
            rotation=[_np.pi/4,_np.pi/4,_np.pi/4],
            translation=[100,100,100], view=True):
    world_solid  = solid.Box("world_solid", 1000, 1000, 1000)
    world_volume = Volume(None, None, world_solid, "world_volume",
                          None, 0, False, "G4_Galactic")

    box_solid   = solid.Box("box_solid", 100, 100, 100)
    other_solid = example_solid(solid_type)

    # union
    union_solid   = solid.Union("union_solid", box_solid,
                                other_solid, [rotation, translation])
    union_volume  = Volume([0, 0, 0], [-300, 0, 0], union_solid,
                           "union_solid", world_volume, 0, False,"G4_Cu")
    # intersection
    intersection_solid   = solid.Intersection("intersection_solid",
                                              box_solid,
                                              other_solid,
                                              [rotation, translation])
    intersection_volume  = Volume([0, 0, 0], [0, 0, 0],
                                  intersection_solid,
                                  "intersection_solid",
                                  world_volume, 0, False, "G4_Cu")
    # subtraction
    subtraction_solid  = solid.Subtraction("subtraction_solid",
                                           box_solid,
                                           other_solid,
                                           [rotation, translation])
    subtraction_volume = Volume([0, 0, 0], [300, 0, 0],
                                subtraction_solid,
                                "subtraction_solid",
                                world_volume, 0, False, "G4_Cu")

    # clip world volume
    extent = world_volume.setClip()

    print extent

    # view output
    if view:
        v1 = VtkViewer()
        v1.addSource(world_volume.pycsgmesh())
        v1.view()

    # If we're testing rotations then write the rotation + translation
    if solid_type == "rotation_test":
        output_name = "boolean_{}_{}_{}".format("rotato",
                                                _np.degrees(rotation),
                                                translation)
    else:
        output_name = "boolean_{}".format(solid_type)
    # gdml output
    g = Gdml()
    g.add(world_volume)
    g.write((_output_path(output_name)))

    # gmad output
    _write_test_gmad(output_name)

def example_solid(solid_type):
    """
    Return an example solid of the type solid_type.

    """
    solid_types = ["box","trap","tubs","cuttubs","orb",
                   "sphere","polycone","para","cons",
                   "trd","paraboloid","ellipsoid","torus",
                   "extrudedsolid","twistedbox","tet",
                   "ellipticaltube","ellipticalcone",
                   "hype","polyhedra", "rotation_test", "world"]
    # Convert input solid_type string to lower case, i.e. make
    # solid_type case insensitive.
    solid_type = solid_type.lower()
    if solid_type not in solid_types:
        raise TypeError("Solid type \"{}\" not recognised!".format(solid_type))
    if solid_type=="box":
        return solid.Box("box_solid",50,50,50)
    elif solid_type == "tubs":
        return solid.Tubs("tubs_solid",10,50,50,0,4.75)
    elif solid_type == "cuttubs":
        return solid.CutTubs("cuttubs_solid",
                               10, 50, 50, 0, 3.14,
                               [-1.0, 0, -1.0], [0, 1, 1])
    elif solid_type == "cons":
        return solid.Cons("cons_solid", 5, 10, 20, 25, 40, 0, (4./3.)*_np.pi)
    elif solid_type == "orb":
        return solid.Orb("ord_solid", 100)
    elif solid_type == "sphere":
        return solid.Sphere("sphere_solid", 40., 100., 0, _np.pi*2,
                              0, _np.pi*1.9, nslice=16, nstack=8)
    elif solid_type == "polycone":
        return solid.Polycone("polycone_solid", _np.pi/4.,
                                3*_np.pi/2,
                                [5*5,5*7,5*9,5*11,5*25,5*27,5*29,5*31,5*35],
                                [0,0,0,0,0,0,0,0,0],
                                [0,5*10,5*10,5*5,5*5,5*10,5*10,5*2,5*2])
    elif solid_type == "polyhedra":
        return solid.Polyhedra("polyhedra_solid",
                                 0., 2*_np.pi, 5, 7,
                                 [0, 5*5,5*8,5*13,5*30,5*32,5*35],
                                 [0, 0, 0, 0, 0, 0, 0, 0, 0],
                                 [0, 5*15, 5*15, 5*4, 5*4, 5*10, 5*10])
    elif solid_type == "para":
        return solid.Para("para_solid", 30, 40, 60, 0.05, 0.1, 0.1)
    elif solid_type == "trd":
        return solid.Trd("trd_solid", 30, 10, 40, 15, 60)
    elif solid_type == "paraboloid":
        return solid.Paraboloid("paraboloid_solid", 3*20., 3*20., 3*35.)
    elif solid_type == "ellipsoid":
        return solid.Ellipsoid("anellipsoid", 40., 60., 200., -10., 70.)
    elif solid_type == "torus":
        return solid.Torus("torus_solid", 0., 60., 80, _np.pi,
                             _np.pi/4, nslice=16, nstack=8)
    elif solid_type == "extrudedsolid":
        polygon_pts      = [[-30, -30], [-30, 30], [30,30], [30,-30], [15,-30],
                            [15,15], [-15,15], [-15,-30]]
        zslices          = [[-60, [0, 30], 0.8], [-15, [0, -30], 1.],
                            [10, [0, 0], 0.6], [60, [0, 30], 1.2]]
        return solid.ExtrudedSolid("extruded_solid", polygon_pts, zslices)
    elif solid_type == "twistedbox":
        return solid.TwistedBox("twistedbox_solid",
                                  30 * 2*_np.pi/360, 30., 40., 60.)
    elif solid_type == "tet":
        return solid.Tet("atet", [0.,0., 50*_np.sqrt(3)],
                           [0.,50*2*_np.sqrt(2./3.),
                            -50*1/_np.sqrt(3)],
                           [-50*_np.sqrt(2),-50*_np.sqrt(2./3.),
                            -1*10/_np.sqrt(3)],
                           [50*_np.sqrt(2),
                            -50*_np.sqrt(2./3.),
                            -50*1/_np.sqrt(3)])
    elif solid_type == "ellipticaltube":
        return solid.EllipticalTube("ellipticaltube_solid", 20., 40., 50.)
    elif solid_type == "ellipticalcone":
        return solid.EllipticalCone("ellipticalcone_solid", 20., 40., 80., 50.)
    elif solid_type == "hype":
        return solid.Hype("hype_solid", 20., 30., 0.7, 0.7, 50.,
                            nslice=16, nstack=8)
    elif solid_type == "trap":
        return solid.Trap("trap_solid", 60., 20*_np.pi/180.,
                            5*_np.pi/180., 40, 30., 40.,
                            10*_np.pi/180, 16, 10., 14., 10*_np.pi/180.)
    # cheekily co-opting this function for testing boolean rotations...
    elif solid_type == "rotation_test":
        return Cons("cons_solid", 0.0, 60, 0.0, 40, 80,
                    0.5 * _np.pi, 0.9 * 2 * _np.pi)
    elif solid_type == "world":
        return solid.Box("world_solid",100000,100000,100000)

def volume() :

    # create solids
    world_solid   = solid.Box("b0", 50, 50, 75)
    box1_solid    = solid.Box("b1", 10, 10, 10)
    box2_solid    = solid.Box("b2", 10, 10, 10)
    box3_solid    = solid.Box("b3", 10, 10, 10)
    box4_solid    = solid.Box("b4", 1, 1, 1)
    box5_solid    = solid.Box("b5", 1, 1, 1)
    box6_solid    = solid.Box("b6", 1, 1, 1)
    tub1_solid = solid.CutTubs("t1", 2, 5, 3, 0, 5.3, [-0.1, 0, -1.0], [0, 0.1, 1])

    # create structure
    world_volume  = Volume([0, 0, 0], [0, 0, 0], world_solid,
                           "world_volume", None, 0, False, "G4_Galactic")
    box1_volume = Volume([0, 0, 0], [0, 0, 0], box1_solid,
                         "box1_volume", world_volume, 0, False, "G4_Cu")
    box2_volume = Volume([0, 0, 0],  [0, 0, 25],  box2_solid,
                         "box2_volume",  world_volume,  0,  False, "G4_Cu")
    box3_volume = Volume([0, 0, _np.pi/4], [0, 0, 50], box3_solid,
                         "box3_volume", world_volume, 0, False, "G4_Galactic")
    box4_volume = Volume([0, 0, 0], [-10, 0, 0], box4_solid,
                         "box4_volume", box3_volume, 0, False, "G4_Al")
    box5_volume = Volume([0, 0, 0], [ 0, 0, 0], box5_solid,
                         "box5_volume", box3_volume, 0, False, "G4_Al")
    box6_volume = Volume([0, 0, _np.pi/4], [ 10, 0, 0], box6_solid,
                         "box6_volume", box3_volume, 0, False, "G4_Cu")
    tub1_volume = Volume([0, 0, 0], [0, 0, 75], tub1_solid,
                         "tub1_volume", world_volume, 0, False, "G4_Cu")

    # clip the world volume
    world_volume.setClip()

    # mesh for visualisation
    m = world_volume.pycsgmesh()
    v1 = VtkViewer()
    v1.addSource(m)
    # v1.view()

    # gdml output
    g = Gdml()
    g.add(world_volume)
    g.write(_output_path('volume'))

    # return for command line debugging
    return world_volume

def overlap() :
    world_solid   = solid.Box("b0", 100, 100, 100)
    world_volume  = Volume([0,0,0],[0,0,0],world_solid,"world_volume",None,0,False,"vacuum")

    box1_solid = solid.Box("b1", 10, 10, 10)
    box1_volume = Volume([0,0,0], [0, 0, 0], box1_solid, "box1_volume", world_volume, 0, False, "copper")

    box2_solid = solid.Box("b2", 10, 10, 10)
    box2_volume = Volume([0,0,0.785], [0, 0, 7.5], box2_solid, "box2_volume", world_volume, 0, False, "copper")

    # clip the world logical
    world_volume.setClip()

    # make solid meshes
    m  = world_volume.pycsgmesh()
    # make overlap meshes
    mo = pycsg_overlap(m)

    # change view setttings of meshes
    mfl = [] # flat mesh list
    for me in flatten(m) :
        me.alpha = 0.25
        mfl.append(me)

    # view
    v1 = VtkViewer()
    v1.addSource(mfl)
    v1.addSource(mo)
    v1.view()

def rotation(x=0, y=0, z=0, view=True, write=True):
    """
    Perform a rotation on a segmented cone.

    Parameters
    ----------

    x - Rotation (in degrees) about the x-axis.
    y - Rotation (in degrees) about the y-axis.
    z - Rotation (in degrees) about the z-axis.
    view - Show mesh.
    write - Write corresponding gdml and gmad file.

    """
    w = Box("world", 10000, 10000, 10000)
    world_volume = Volume([0,0,0],
                          [0,0,0],
                          w, "world-volume", None,
                          1, False, "G4_NITROUS_OXIDE")

    cone = Cons("my_cone",
                0.0,  # inner radius of face1
                60,  # outer radius of face1
                0.0,  # inner radius of face2
                40,  # outer radius of face2
                80,  # half-length of cone
                0.0,  # start angle of rotation
                3*_np.pi/2) # angular length of segment

    cone_vol = Volume([_math.radians(x),
                       _math.radians(y),
                       _math.radians(z)],
                      [0,0,0],
                      cone,
                      "cone",
                      world_volume,
                      1,
                      False,
                      "G4_NITROUS_OXIDE")
    world_volume.setClip()
    if view:
        mesh = world_volume.pycsgmesh()
        viewer = VtkViewer()
        viewer.addSource(mesh)
        viewer.view()
    if write:
        g = Gdml()
        g.add(world_volume)
        file_name = "rotation_%g_%g_%g_deg" % (x, y, z)
        path = _output_path(file_name)
        _write_test_gmad(file_name)
        g.write(path)

def rotate_about_x(x=45):
    """
    Rotate a segmented cone section about the x-axis.
    Default: 45 degrees.

    """
    rotation(x=x)

def rotate_about_y(y=45):
    """
    Rotate a segmented cone section about the y-axis.
    Default:  45 degrees.

    """
    rotation(y=y)

def rotate_about_z(z=45):
    """
    Rotate a segmented cone section about the z-axis.
    Default: 45 degrees.

    """
    rotation(z=z)

def no_rotation():
    rotation()

def boolean_rotation(xrot=0, yrot=0, zrot=0, x=0, y=0, z=0):
    """
    For testing the rotation in boolean solids.  Rotations in DEGREES.

    xrot,yrot,zrot rotations of second solid with respect to first in DEGREES.

    """
    rot = _np.radians([xrot, yrot, zrot])
    tra = [x,y,z]
    boolean(solid_type="rotation_test",  rotation=rot, translation=tra)

def tbxyz2matrix2tbyz():
    """
    Test that the
    Start with randomly generated tait-bryan angles, convert to a
    rotation matrix, then back to tait bryan angles, then back to a
    rotation matrix, and then compare the two rotation matrices.
    """
    tait_bryan_angles=list(_np.random.rand(3) * 2 * _np.pi)
    rotation_matrix = tbxyz2matrix(tait_bryan_angles)
    tait_bryan_angles_new = matrix2tbxyz(rotation_matrix)
    rotation_matrix_new = tbxyz2matrix(tait_bryan_angles_new)

    assert _np.allclose(rotation_matrix,rotation_matrix_new)

def relative_rot():
    """
    Test for calculating relative rotations.

    If no assertion error, then successful.

    """
    # Generates two random pairs of Tait-Bryan (x,y,z) angles,
    tait_bryan_angles_1 = list(_np.random.rand(3) * 2 * _np.pi)
    tait_bryan_angles_2 = list(_np.random.rand(3) * 2 * _np.pi)

    # Converts to rotation matrices.
    matrix_1 = tbxyz2matrix(tait_bryan_angles_1)
    matrix_2 = tbxyz2matrix(tait_bryan_angles_2)

    # Get the relative Tait-Bryan angle that goes from first to second
    relative_tbxyz = relative_rotation(tait_bryan_angles_1,
                                       tait_bryan_angles_2)

    # Convert this to a matrix
    relative_matrix = tbxyz2matrix(relative_tbxyz)

    # Assert that the first matrix multiplied by matrix that maps the
    # first to the second results in the second.
    assert _np.allclose(matrix_1 * relative_matrix, matrix_2)

def material():
    # monoisotopic element:
    aluminium_27 = Isotope("Aluminium27", 13, 27, 26.9815)
    aluminium = Element("Aluminium", aluminium_27)

    # polyisotopic element:
    oxygen16 = Isotope("Oxygen_16", 8, 16, 15.9949)
    oxygen17 = Isotope("Oxygen_17", 8, 17, 16.9991)
    oxygen18 = Isotope("Oxygen_18", 8, 18, 17.9992)

    # Fractions are relative abundances
    oxygen = Element("Oxygen", [oxygen16*0.9976,
                                oxygen17*0.0004,
                                oxygen18*0.002])

    # Setting the mean excitation energy for Aluminium Oxide.
    aluminium_MEE = MaterialProperty("MEE", unit="eV", value=145.200000)

    # Aluminium Oxide material.  Component fractions are mass fractions.
    aluminium_oxide = Material(name="AluminiumOxide",
                               density=3.97, # g/cm3
                               components=[aluminium * 0.529251,
                                           oxygen * 0.470749],
                               state="solid",
                               properties=aluminium_MEE)
    # aluminium /material/
    aluminium_mat = Material(name="Aluminium",
                             density=2.7,
                             components=aluminium,
                             state="solid")

    # An aluminium oxide world solid:
    world_solid   = solid.Box("b0", 100, 100, 100)
    world_volume  = Volume([0,0,0],[0,0,0],world_solid,
                           "world_volume", None, 0, False, aluminium_oxide)

    # If for some reason you want to just use a MaterialReference at
    # the top level (instead of a string), then you can:
    laughing_gas = MaterialReference("G4_NITROUS_OXIDE")
    laughing_orb = Orb("hehe_ORB", 5.0)
    laughing_volume = Volume([0,0,0], [0,0,0], laughing_orb,
                             "laughing_volume", world_volume, 0,
                             False, laughing_gas)

    # If you want to construct a material out of other materials, both
    # personally defined and built-in:
    blood = MaterialReference("G4_BLOOD_ICRP")
    aluminium_blood = Material("aluminium_blood",
                              1.0,
                              [0.1*aluminium_mat,
                               0.9*blood],
                              state="gas", # to show writing pressure
                              temperature=1000)
    red_cube = Box("red_cube", 5.0, 5.0, 5.0)
    red_placement = Volume([0,0,0], [15.0,0,0], red_cube,
                           "red_volume", world_volume, 0,
                           False, aluminium_blood)

    aluminium_blood.add_component(laughing_gas * 0.3)
    # Always have to clip otherwise you can't write...

    world_volume.setClip()
    out = Gdml()
    out.add(world_volume)
    out.write(_output_path("material"))

def _output_path(name):
    path = (_path.dirname(__file__)
            + "/testOutput/"
            + name
            + ".gdml")
    return path

def _write_test_gmad(name):
    output_dir = _path.dirname(__file__) + "/testOutput/"
    gmad_path = output_dir + name + ".gmad"
    gdml_path = output_dir + name + ".gdml"
    with open(gmad_path, 'w') as gmad:
        gmad.write("test_component: element, l=10.*m, geometry=\"gdml:%s\","
                   " outerDiameter=2.1*m;\n" % gdml_path)
        gmad.write('\n')
        gmad.write("component : line = (test_component);\n")
        gmad.write('\n')
        gmad.write("beam,  particle=\"e-\",\n"
                   "energy=1.5 * GeV,\n"
                   "X0=0.1*um;\n")
        gmad.write('\n')
        gmad.write("use, period=component;\n")
