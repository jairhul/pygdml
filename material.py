from copy import copy as _copy

"""
Overview
--------

This module represents GDML  materials in python.

The three classes, Material, Isotope, and Element correspond to those
found in GDML.

Materials can be defined in terms of materials and elements.  Elements
in turn are defined in terms of isotopes.  Isotopes are the "atomic"
structures.

To support built-in materials and elements provided by, for example,
Geant4, the classes MaterialReference and ElementReference are
provided.  These are constructed with just a string, but include
useful functionality for fractions when creating materials out of
these external definitions.

As Materials can include arbitrary "properties", the class
MaterialProperty is provided for writing these to Material
definitions.  This amounts to a tag name followed by at least one
attribute.

All names (except for References) are mangled and guaranteed to be
unique per instance when written to GDML using gdml.py.

Examples can be found in test.py.

"""

STANDARD_PRESSURE = 101.325e5
NTP_TEMPERATURE = 293.15

class _MaterialBase(object):
    def number_of_child_components(self):
        """
        Recursively counts the number of components that make up this
        material.
        """
        # Subtract one because flatten_material includes self.
        return len(flatten_material(self)) - 1

    def component_names_and_fractions(self):
        """
        Get the  names and fractions of the components that make up this object.
        """
        if isinstance(self.components, list):
            return [(component.name, component.fraction)
                    for component in self.components]
        else:
            return [(self.components.name, 1.0)]


class Material(_MaterialBase):
    """
    A class for the GDML Material element.

    A material describes the macroscopic properties of the matter,
    e.g. temperature, pressure, state, density, radiation length,
    etc...

    This description works on the basic assumption that material
    elements have depth equal to one.  That is, a material is a
    sequence of XML elements, each with no elements of its own, only
    attributes.  Don't expect this to be a problem...

    Parameters
    ----------

    name : Name of the material
    state : "solid" OR "liquid" OR "gas" OR "undefined"
    components : Isotope, or Material, or Element instance or list
                 thereof.  Mass fractions denoted with * operator.
    temperature : in Kelvin.  Default: NTP (293.15K)
    pressure : in Pascals.  Default: STP (101.325e5)
    """
    def __init__(self,
                 name,
                 density,
                 components,
                 state="undefined",
                 temperature=NTP_TEMPERATURE,
                 pressure=STANDARD_PRESSURE,
                 properties=None):

        self.name = name
        self.density = density
        self.components = components
        if state not in {"solid", "liquid", "gas", "undefined"}:
            raise TypeError("Unknown state: %s" % state)
        self.state = state
        self.temperature = temperature
        self.pressure = pressure
        self.properties = properties
        self.fraction = 1.0

    @property
    def components(self):
        return self._components

    @components.setter
    def components(self, components):
        # Either an iterable of components or a single component.  Component
        # fractions must sum to 1.
        if isinstance(components, list):
            total = sum([constituent.fraction
                         for constituent in components])
        elif isinstance(components, (Material, Isotope, Element,
                                     MaterialReference, ElementReference)):
            total = components.fraction
        else:
            raise TypeError("Unknown constituent type: %s" % type(components))

        if total == 1.0:
                self._components = components
        else:
            raise ValueError("Fractions do not add up to 1.0.  Sum: %s" % total)

    @property
    def properties(self):
        return self._properties

    @properties.setter
    def properties(self, properties):
        if (isinstance(properties, (list, MaterialProperty))
            or properties == None):
            self._properties = properties
        else:
            raise TypeError("properties should be a MaterialProperty"
                            " instance of a list of instances")

    def add_component(self, new_component):
        if isinstance(self.components, (Material, Isotope, Element,
                                        MaterialReference, ElementReference)):
            old_component = self.components
            old_component.fraction = 1 - new_component.fraction
            self.components = [old_component, new_component]
        else:
            old_components = self.components
            for component in old_components:
                component.fraction *= 1 - new_component.fraction
            old_components.append(new_component)
            assert sum(comp.fraction for comp in self.components) == 1

    def __mul__(self, fraction):
        if (fraction < 0.0 or fraction > 1.0):
            raise ValueError("Fraction out of bounds.  Must be"
                             " between 0.0 and 1.0.")
        result = _copy(self)
        result.fraction *= fraction
        return result

    def __rmul__(self, fraction):
        if (fraction < 0.0 or fraction > 1.0):
            raise ValueError("Fraction out of bounds.  Must be"
                             " between 0.0 and 1.0.")
        result = _copy(self)
        result.fraction *= fraction
        return result

    def _get_number_of_properties(self):
        if isinstance(self.properties, MaterialProperty):
            return 1
        elif isinstance(self.properties, list):
            return len(self.properties)
        else:
            return 0

    def __repr__(self):
        # Basically, don't print pressure unless it's a gas.
        first_part = (("<Material: %s, density=%s g/cm3, No. of components=%s, "
                       "state=%s, temperature=%s K, ")
                      % (self.name, self.density,
                         self.number_of_child_components(),
                         self.state, self.temperature))

        third_part =  "No. of properties=%s>" % self._get_number_of_properties()

        if self.state == "gas":
            return (first_part
                    + ("pressure=%s Pa, " % self.pressure)
                    + third_part)
        else:
            return first_part + third_part

    def __iter__(self):
        if isinstance(self.components, (Material, Element,
                                        ElementReference,
                                        MaterialReference)):
            yield self.components
        else:
            for component in self.components:
                yield component

    def __eq__(self, other):
        # Test equality of everything except for fraction, as a
        # differing fraction alone is not a whole new material type.
        if type(other) == Material:
            return (self.name == other.name
                    and self.density == other.density
                    and self.components == other.components
                    and self.state == other.state
                    and self.temperature == other.temperature
                    and self.pressure == other.pressure
                    and self.properties == other.properties)
        else:
            return False


class MaterialProperty(object):
    """
    Class for representing a Material property.
    Parameters
    ----------

    variable : Name of variable as read by GDML.  E.g. "T" for
               Temperature (NOT "temperature"), "MEE" for Mean
               Exictation Energy, "D" for Density, etc.
    value    : The value assigned to this variable.
    """
    def __init__(self, variable, **kwargs):
        self.variable = variable
        self.attributes = kwargs


class Isotope(object):
    """GDML Isotope class.

    Parameters
    ----------

    name          : name of isotope
    atomic_number : isotope's atomic number (Z)
    mass_number   : isotope's mass number (N)
    molar_mass    : Molar mass in grams/mole.

    Examples
    --------

    >>> hydrogen_1 = Isotope("H1", 1, 1, 1.00782503081372)

    To get this isotope as a fraction, do, for example:

    >>> 0.5*hydrogen_1

    Relative abundances must be between 0.0 and 1.0, and only have
    meaning when used to define another material or element.

    """
    def __init__(self, name,
                 atomic_number,
                 mass_number,
                 molar_mass):
        self.name = name
        self.atomic_number = atomic_number
        self.mass_number = mass_number
        self.molar_mass = molar_mass
        self.fraction = 1.0

    def __mul__(self, fraction):
        if (fraction < 0.0 or fraction > 1.0):
            raise ValueError("Fraction out of bounds.  Must be"
                             " between 0.0 and 1.0.")
        result = _copy(self)
        result.fraction *= fraction
        return result

    def __rmul__(self, fraction):
        if (fraction < 0.0 or fraction > 1.0):
            raise ValueError("Fraction out of bounds.  Must be"
                             " between 0.0 and 1.0.")
        result = _copy(self)
        result.fraction *= fraction
        return result

    def __repr__(self):
        return ("<Isotope: \"%s\", Z=%s, A=%s, M=%s,"
                " fraction=%s>"
                    % (self.name, self.atomic_number,
                       self.mass_number, self.molar_mass, self.fraction))

    def __eq__(self, other):
        # Basically purpose here is to ignore differences in fraction,
        # as two defintions identical in everyway except "fraction",
        # are really the same definition, just in a different context.
        if type(other) is Isotope:
            return (self.name == other.name
                    and self.atomic_number == other.atomic_number
                    and self.mass_number == other.mass_number
                    and self.molar_mass == other.molar_mass)
        else:
            return False


class Element(_MaterialBase):
    """
    A class for "Element" in GDML.  This class must be instantiated
    with use of the Isotope class.

    Parameters
    ----------

    name : name of the element, e.g. "Hydrogen"
    components : A single isotope or a list of isotopes with the
                 appropriate relative abundances.

    Examples
    --------
    >>> aluminium_27 = Isotope("Aluminium", 13, 27, 26.9815)
    >>> aluminium = Element("Aluminium", "Al", aluminium_27)


    >>> hydrogen_1 = Isotope("H1", 1, 1, 1.00782503081372)
    >>> hydrogen_2 = Isotope("H2", 1, 2, 2.01410199966617)
    >>> hydrogen = Element("Hydrogen", "H", [hydrogen_1 * 0.9998,
                                             hydrogen_2 * 0.02])

    Trying to create an element in which the fractions do not add up
    to 1.0 will result in an error.

    """

    def __init__(self, name, components):
        self.name = name
        self.components = components
        self.fraction = 1.0

    @property
    def components(self):
        return self._components

    @components.setter
    def components(self, components):
        if isinstance(components, list):
            total = sum([constituent.fraction
                         for constituent in components])
        elif isinstance(components, Isotope):
            total = components.fraction
        else:
            raise TypeError("Unknown constituent type: %s"
                            % type(components))

        if total == 1.0:
                self._components = components
        else:
            raise ValueError("Fractions do not add up to 1.0.  Sum: %s"
                             % total)


    def __mul__(self, fraction):
        if (fraction < 0.0 or fraction > 1.0):
            raise ValueError("Fraction out of bounds.  Must be"
                             " between 0.0 and 1.0.")
        result = _copy(self)
        result.fraction *= fraction
        return result

    def __rmul__(self, fraction):
        if (fraction < 0.0 or fraction > 1.0):
            raise ValueError("Fraction out of bounds.  Must be"
                             " between 0.0 and 1.0.")
        result = _copy(self)
        result.fraction *= fraction
        return result

    def __repr__(self):
        if isinstance(self.components, list):
            number_of_components = len(self.components)
        else:
            number_of_components = 1
        element_string = ("<Element: \"%s\", fraction=%s, No. of"
                          " components=%s>"
                          % (self.name, self.fraction, number_of_components))

        return element_string

    def __iter__(self):
        # If components is a single instance of some other material type:
        if isinstance(self.components, (Element,
                                        Isotope,
                                        ElementReference,
                                        MaterialReference)):
            yield self.components
        else: # Else iterate over the list of components.
            for component in self.components:
                yield component

    def __eq__(self, other):
        if type(other) is Element:
            return (self.name == other.name
                    and self.components == other.components)
        else:
            return False


class MaterialReference(object):
    """
    Class that allows for representing externally defined or built-in
    material definitions such as those provided by geant4.
    Mass fractions denoted, with "*".

    Parameters
    ----------

    name : name of the reference, e.g. "G4_Galactic".
    """
    def __init__(self, name):
        self.name = name
        self.fraction = 1.0

    def __mul__(self, fraction):
        if (fraction < 0.0 or fraction > 1.0):
            raise ValueError("Fraction out of bounds.  Must be"
                             " between 0.0 and 1.0.")
        result = _copy(self)
        result.fraction *= fraction
        return result

    def __rmul__(self, fraction):
        if (fraction < 0.0 or fraction > 1.0):
            raise ValueError("Fraction out of bounds.  Must be"
                             " between 0.0 and 1.0.")
        result = _copy(self)
        result.fraction *= fraction
        return result

    def __eq__(self, other):
        if type(other) is MaterialReference:
            return (self.name == other.name)
        else:
            return None

    def __repr__(self):
        return "<MaterialReference: \"%s\">" % self.name


class ElementReference(object):
    """
    Class that allows for representing externally defined or built-in
    element definitions such as those provided by geant4.
    Mass fractions denoted, with "*".

    Parameters
    ----------

    name : name of the reference, e.g. "G4_Galactic".
    """
    def __init__(self, name):
        self.name = name
        self.fraction = 1.0

    def __mul__(self, fraction):
        if (fraction < 0.0 or fraction > 1.0):
            raise ValueError("Fraction out of bounds.  Must be"
                             " between 0.0 and 1.0.")
        result = _copy(self)
        result.fraction *= fraction
        return result

    def __rmul__(self, fraction):
        if (fraction < 0.0 or fraction > 1.0):
            raise ValueError("Fraction out of bounds.  Must be"
                             " between 0.0 and 1.0.")
        result = _copy(self)
        result.fraction *= fraction
        return result

    def __eq__(self, other):
        if type(other) is ElementReference:
            return (self.name == other.name)
        else:
            return None;

    def __repr__(self):
        return "<ElementReference: \"%s\">" % self.name


def flatten_material(material):
    '''
    Reduces a material or element or isotope to all of its constituent
    material/element/isotope definitions.
    '''
    materials = []
    def flatten_material_iter(material):
        # If a Material or Element with a list of components
        if (isinstance(material, (Material, Element))
            and isinstance(material.components, list)):
            # Add the node material.
            materials.append(material)
            # Then recursively add the components
            for component in material.components:
                flatten_material_iter(component)
        # If a Material or Element with a single component
        elif (isinstance(material, (Material, Element))
              and not isinstance(material, list)):
            # Add the node material.
            materials.append(material)
            # Recursively add that component.
            flatten_material_iter(material.components)
        # Else if material is an "atomic" type, then just add it.
        elif isinstance(material, (Isotope,
                                   MaterialReference,
                                   ElementReference)):
            materials.append(material)
    flatten_material_iter(material)
    return materials
