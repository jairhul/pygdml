import numpy as _np

from pycsgMod.geom import Vector as _Vector
from matplotlib.cbook import flatten

from transformation import *

import solid as _solid

class Volume(object) :
    '''
    Class representing logical and physical volumes in Geant4.

    Parameters
    ----------

    rot : A list of three numbers giving the Tait-Bryan rotation in [x,y,z].
          Order of rotation: x -> y -> z
          A positive value corresponds to a clockwise rotation looking
          AT/against the direction of the axis.
    tlate : A list of three numbers giving the translation in [x,y,z].
    currentVolume : A solid to be placed with this volume.
    name : The name of this volume.
    motherVolume : The volume that this is to be placed in.
    copyNr : doesn't do anything.
    surfCheck : doesn't do anything.
    material : Material of the resulting logical volume.
    scale : scale in [x,y,z] (?)
    '''
    def __init__(self, rot, tlate, currentVolume, name, motherVolume,
                 copyNr, surfCheck, material, scale = [1,1,1]):


        origin = [0,0,0]

        if rot is not None :
            self.rot            = rot
        else :
            self.rot            = [0,0,0]

        if tlate is not None :
            self.tlate          = tlate
        else :
            self.tlate          = [0,0,0]

        self.scale              = scale

        # TODO : Should this be relabelled to currentSolid
        self.currentVolume           = currentVolume
        self.currentVolume.material  = material
        self.name                    = name
        self.motherVolume            = motherVolume
        self.copyNr                  = copyNr
        self.surfCheck                = surfCheck
        self.material                = material

        self.daughterVolumes = []
        self.daughterMeshes  = []

        # add volume to mother's list of daughters
        if motherVolume != None :
            motherVolume.add(self)

        # keep a ref to the mother volume
        self.motherVolume = motherVolume

    def setOrigin(self, origin):
        self.origin = origin
        for dv in self.daughterVolumes :
            dv.tlate = list(-_np.array(self.origin)+_np.array(dv.tlate))

        # Move the beam pipe if a subtraction solid
        if isinstance(self.currentVolume,_solid.Subtraction) :
            self.currentVolume.tra2[1] = self.currentVolume.tra2[1] - _np.array(self.origin)

    def setSize(self,size):
        # print 'setSize'
        self.size = size

        # print type(self.currentVolume)

        # if a box
        if isinstance(self.currentVolume,_solid.Box) :
            self.currentVolume.pX = size[0] / 2.
            self.currentVolume.pY = size[1] / 2.
            self.currentVolume.pZ = size[2] / 2.
        elif isinstance(self.currentVolume,_solid.Subtraction) :
            self.currentVolume.obj1.pX = size[0] / 2.
            self.currentVolume.obj1.pY = size[1] / 2.
            self.currentVolume.obj1.pZ = size[2] / 2.
            
    def getSize(self):
        m = self.pycsgmesh()
        extent = _np.array(pycsg_extent(m))

        # Center geometry in gdml
        size = extent[1] - extent[0]
        centre = extent[1] - size / 2

        return [size,centre,extent]

    def setClip(self):
        [self.size, self.centre, self.extent] = self.getSize()

        self.setSize(self.size)
        self.setOrigin(self.centre)

        return [self.size, self.centre, self.extent]

    def add(self, volume) :
        self.daughterVolumes.append(volume)

    def __repr__(self) :
        return "<{}: name=\"{}\", currentVolume=\"{}\">".format(
            type(self).__name__,
            self.name,
            self.currentVolume.name)

    def pycsgmesh(self):
        # return list of meshes
        daughterMeshes = []
        for dv in self.daughterVolumes :
            dvm = dv.pycsgmesh()
            daughterMeshes.append(dvm)

            # transform meshes
            # print 'Volume.pycsgmesh>',self.name, self.tlate, dv.name, dv.rot, dv.tlate, dvm

        map_nlist(daughterMeshes,self.tlate,tbxyz(self.rot),self.scale)


        #if len(self.daughterVolumes) == 0 :
        if True :
            m = self.currentVolume.pycsgmesh()
            r = tbxyz(self.rot)
            m.scale(self.scale)
            m.rotate(r[0],rad2deg(r[1]))
            m.translate(self.tlate)
            if len(self.daughterVolumes) != 0 :
                m.type  = 'logical'
                m.alpha = 0.25
            if self.motherVolume == None :
                m.type  = 'world'
            daughterMeshes.insert(0,m)

        return daughterMeshes

def map_nlist(nlist,trans,rot,scale = [1,1,1]):
    '''Function to apply transformation (rot then trans) to nested list of meshes (nlist)'''
    for i in range(len(nlist)) :
        if isinstance(nlist[i],list) :
            map_nlist(nlist[i],trans,rot,scale)
        else :
            nlist[i].scale(scale)
            nlist[i].rotate(rot[0],rad2deg(rot[1]))
            nlist[i].translate(trans)

def scale(mesh, scale=[1,1,1]) :
    '''Function to scale CSG, not implemented in pycsg, could move there'''
    for poly in mesh.polygons:
        for v in poly.vertices:
            v.pos = [scale[0]*v.pos[0],scale[1]*v.pos[1],scale[2]*v.pos[2]]

def pycsg_extent(meshTree) :
    '''Function to determine extent of an tree of meshes'''

    vMin = _Vector([1e50,1e50,1e50])
    vMax = _Vector([-1e50,-1e50,-1e50])

    for m in flatten(meshTree) :
        try :
            if m.type == 'world' :
                continue
        except AttributeError :
            pass
        polys = m.toPolygons()
        for p in polys :
            for vert in p.vertices :
                v = vert.pos
                if v[0] < vMin[0] :
                    vMin[0] = v[0]
                if v[1] < vMin[1] :
                    vMin[1] = v[1]
                if v[2] < vMin[2] :
                    vMin[2] = v[2]
                if v[0] > vMax[0] :
                    vMax[0] = v[0]
                if v[1] > vMax[1] :
                    vMax[1] = v[1]
                if v[2] > vMax[2] :
                    vMax[2] = v[2]

    return [vMin,vMax]

def pycsg_overlap(meshTree) :
    '''Function to determine if there overlaps of meshes'''
    mil = [] # mesh intersection list

    # count number of meshes and make flat list
    imesh = 0
    mfl   = [] # mesh flat list
    for m in flatten(meshTree) :
        # print m
        try :
            if m.type != 'logical' :
                pass
        except AttributeError :
            mfl.append(m)
            imesh += 1


    # loop over meshes and determine intersection, if intersect append to intersecting mesh list
    for i in range(imesh) :
        for j in range(i+1,imesh) :
            m1 = mfl[i]
            m2 = mfl[j]
            mi = m1.intersect(m2) # mesh intersection
            mil.append(mi)

    return mil

