import os
import warnings
from uuid import uuid4 as _uuid4
from collections import OrderedDict as _OrderedDict
from collections import defaultdict
from xml.dom import minidom as _minidom
from xml.dom import getDOMImplementation
from ast import literal_eval
from math import pi
from volume import *
from solid import *
from material import *


class Gdml :
    def __init__(self):
        self.constants    = {}
        self.positions    = {}
        self.quantities   = {}
        self.rotations    = {}
        self.solids       = {}
        self.gdmlvols     = {}
        self.gdmlphvols   = defaultdict(list)
        self.worldvolume  = str()
        self.volumes      = []
        self.exclude      = [] #parametrized volumes not converted
        self.rootVolume   = None
        self.rootNode     = None

    def load(self, filename):
        data  = open(filename)
        #remove all newline charecters and whitespaces outside tags
        fs = str()
        for l in data:
            l = l.strip()
            if(l.endswith(">")):
                end=""
            else:
                end=" "
            if(len(l) != 0):
                fs += (l+end)

        xmldoc = _minidom.parseString(fs)

        self.parseDefines(xmldoc)
        self.parseSolids(xmldoc)
        self.parseStructure(xmldoc)

    def parseDefines(self, xmldoc):
        self.structure = xmldoc.getElementsByTagName("define")[0]

        for df in self.structure.childNodes :
            define_type  = df.tagName
            name         = df.attributes["name"].value
            def_attrs    = df.attributes

            keys       = df.attributes.keys()
            vals       = [attr.value for attr in df.attributes.values()]
            def_attrs  = {key: val for (key,val) in zip(keys, vals)}

            if(define_type == "constant"):
                self.constants[name]=df.attributes["value"].value
            elif(define_type == "position"):
                self.positions[name]=self._getCoordinateList(def_attrs)
            elif(define_type == "quantity"):
                self.quantities[name] =self._get_var("value", float, "atr", **def_attrs)
            elif(define_type == "rotation"):
                self.rotations[name]=self._getCoordinateList(def_attrs)
            else:
                print "Urecognised define: ", define_type

    def parseSolids(self, xmldoc):
        solids_list = []
        self.xmlsolids = xmldoc.getElementsByTagName("solids")[0]

        for sd in self.xmlsolids.childNodes :
            csg_solid_types = ["subtraction", "union", "intersection"]
            ply_solid_types = ["polycone", "polyhedra"]
            solid_type = sd.tagName

            if (solid_type in csg_solid_types): #need to inspect child nodes to get all parameters for csg solids
                keys = sd.attributes.keys()
                vals = [attr.value for attr in sd.attributes.values()]

                for csgsd in sd.childNodes:
                    prm = csgsd.tagName
                    if(prm == "first" or prm == "second"):
                        keys.append(prm)
                        vals.append(csgsd.attributes["ref"].value)
                    elif(prm == "position" or prm == "rotation"):
                        pos_keys = csgsd.attributes.keys()
                        pos_vals = [attr.value for attr in csgsd.attributes.values()]
                        pos = {key: val for (key,val) in zip(pos_keys, pos_vals)}
                        keys.append(prm)
                        vals.append(pos)

                    else:
                        warnings.warn("CSG solid paramets '"+prm+"' unknown")

                gdml_attributes = {key: val for (key,val) in zip(keys, vals)}

            elif (solid_type in ply_solid_types): #need to inspect child nodes to get zplane info for poly solids
                keys = sd.attributes.keys()
                vals = [attr.value for attr in sd.attributes.values()]

                rmin  = []
                rmax  = []
                z     = []

                count = 0
                for zplane in sd.childNodes:
                    tagname = zplane.tagName
                    if(tagname == "zplane"):      #check that its not some other type of child node
                        keys_zpl = zplane.attributes.keys()
                        vals_zpl = [attr.value for attr in zplane.attributes.values()]
                        keys_zpl = [key+"_"+str(count) for key in keys_zpl] #keep track of different z planes
                        keys.extend(keys_zpl)
                        vals.extend(vals_zpl)
                        count = count+1

                    else:
                        warnings.warn("Poly-solid tag '"+tagname+"' unknown")

                keys.append("nzplanes")
                vals.append(count)

                gdml_attributes = {key: val for (key,val) in zip(keys, vals)}

            elif (solid_type == "xtru"):     #the extrusion solid is a special case
                keys = sd.attributes.keys()
                vals = [attr.value for attr in sd.attributes.values()]

                count_vrt = 0
                count_pln = 0
                for xtru_element in sd.childNodes:
                    tagname = xtru_element.tagName
                    if(tagname == "twoDimVertex"):      #check that its not some other type of child node
                        keys_zpl = xtru_element.attributes.keys()
                        vals_zpl = [attr.value for attr in xtru_element.attributes.values()]
                        keys_zpl = [key+"_"+str(count_vrt) for key in keys_zpl] #keep track of different z planes
                        keys.extend(keys_zpl)
                        vals.extend(vals_zpl)
                        count_vrt = count_vrt+1

                    elif(tagname == "section"):      #check that its not some other type of child node
                        keys_zpl = xtru_element.attributes.keys()
                        vals_zpl = [attr.value for attr in xtru_element.attributes.values()]
                        number   = int(xtru_element.attributes["zOrder"].value)
                        keys_zpl = [key+"_"+str(number) for key in keys_zpl] #keep track of different z planes
                        keys.extend(keys_zpl)
                        vals.extend(vals_zpl)
                        count_pln = count_pln+1
                    else:
                        warnings.warn("Extrusion solid tag '"+tagname+"' unknown")

                keys.append("nzplanes")
                vals.append(count_pln)
                keys.append("nverts")
                vals.append(count_vrt)

                gdml_attributes = {key: val for (key,val) in zip(keys, vals)}

            else:
                keys       = sd.attributes.keys()
                vals       = [attr.value for attr in sd.attributes.values()]
                gdml_attributes = {key: val for (key,val) in zip(keys, vals)}

            solid = self._constructCSGSolid(solid_type, gdml_attributes)

            if(solid is not None):
                self.solids[sd.attributes["name"].value] = solid


    def parseStructure(self, xmldoc):
        self.structure = xmldoc.getElementsByTagName("structure")[0]

        self._extractNodeData(self.structure)

        #Find world volume. The world volume is the only one not pointed to by any physical volumes
        for name, attrs in self.gdmlvols.iteritems():
            if name not in self.gdmlphvols:
                if name not in self.exclude:
                    self.worldvolume = name

        #Build the heirarchy.
        #All volumes point to their mother, but not to their daughters, so build the hirearchy in ascending order
        for name, attrs in self.gdmlphvols.iteritems():
            for i in range(len(self.gdmlphvols[name])):
                mother = self.gdmlphvols[name][i][0]
                self.gdmlvols[mother][2].append(name)

        #Recursively build the structure in descending order starting from the world volume
        self._buildPycsgStructure(self.worldvolume, None, verbose=True)


    def _box(self,**kwargs):
        name = self._get_var("name", str, "atr", **kwargs)
        x    = self._get_var("x", float, "lgt", **kwargs)/2
        y    = self._get_var("y", float, "lgt", **kwargs)/2
        z    = self._get_var("z", float, "lgt", **kwargs)/2

        csgsolid = Box(name, x, y, z)
        return csgsolid


    def _para(self,**kwargs):
        name  = self._get_var("name", str, "atr", **kwargs)
        x     = self._get_var("x", float, "lgt",**kwargs)/2
        y     = self._get_var("y", float, "lgt", **kwargs)/2
        z     = self._get_var("z", float, "lgt", **kwargs)/2
        phi   = self._get_var("phi", float, "ang", **kwargs)
        alpha = self._get_var("alpha", float, "ang", **kwargs)
        theta    = self._get_var("theta", float, "ang", **kwargs)

        csgsolid = Para(name, x, y, z, alpha, theta, phi)
        return csgsolid

    def _sphere(self,**kwargs):
        name       = self._get_var("name", str, "atr", **kwargs)
        rmin       = self._get_var("rmin", float, "lgt", **kwargs)
        rmax       = self._get_var("rmax", float, "lgt",**kwargs)
        startphi   = self._get_var("startphi", float, "ang",**kwargs)
        deltaphi   = self._get_var("deltaphi", float, "ang",**kwargs)
        starttheta = self._get_var("starttheta", float, "ang",**kwargs)
        deltatheta = self._get_var("rmin", float, "ang", **kwargs)

        csgsolid = Sphere(name, rmin, rmax, startphi, deltaphi, starttheta, deltatheta)
        return csgsolid

    def _orb(self,**kwargs):
        name = self._get_var("name", str, "atr", **kwargs)
        r    = self._get_var("r", float, "lgt",**kwargs)

        csgsolid = Orb(name, r)
        return csgsolid

    def _cone(self,**kwargs):
        name  = self._get_var("name", str, "atr", **kwargs)
        rmin1 = self._get_var("rmin1", float, "lgt",**kwargs)
        rmax1 = self._get_var("rmax1", float, "lgt",**kwargs)
        rmin2 = self._get_var("rmin2", float, "lgt",**kwargs)
        rmax2 = self._get_var("rmax2", float, "lgt",**kwargs)
        dz    = self._get_var("z", float, "lgt",**kwargs)/2
        sphi  = self._get_var("startphi", float, "ang",**kwargs)
        dphi  = self._get_var("deltaphi", float, "ang", **kwargs)

        csgsolid = Cons(name, rmin1, rmax1, rmin2, rmax2, dz, sphi, dphi)
        return csgsolid

    def _cutTube(self,**kwargs):
        name  = self._get_var("name", str, "atr", **kwargs)
        rmin  = self._get_var("rmin", float, "lgt",**kwargs)
        rmax  = self._get_var("rmax", float, "lgt",**kwargs)
        dz    = self._get_var("z", float, "lgt",**kwargs)/2
        sphi  = self._get_var("startphi", float, "ang",**kwargs)
        dphi  = self._get_var("deltaphi", float, "ang", **kwargs)
        lx    = self._get_var("lowX", float, "lgt",**kwargs)
        ly    = self._get_var("lowY", float, "lgt",**kwargs)
        lz    = self._get_var("lowZ", float, "lgt",**kwargs)
        hx    = self._get_var("highX", float, "lgt",**kwargs)
        hy    = self._get_var("highY", float, "lgt",**kwargs)
        hz    = self._get_var("highZ", float, "lgt",**kwargs)
        lNorm = [lx, ly, lz]
        hNorm = [hx, hy, hz]

        csgsolid = CutTubs(name, rmin, rmax, dz, sphi, dphi, lNorm, hNorm)
        return csgsolid

    def _ellipsoid(self,**kwargs):
        name  = self._get_var("name", str, "atr", **kwargs)
        ax    = self._get_var("ax", float, "lgt", **kwargs)
        ay    = self._get_var("ay", float, "lgt", **kwargs)
        az    = self._get_var("az", float, "lgt", **kwargs)
        bcut  = self._get_var("zcut1", float, "lgt", **kwargs)
        tcut  = self._get_var("zcut2", float, "lgt", **kwargs)

        csgsolid = Ellipsoid(name, ax, ay, az, bcut, tcut)
        return csgsolid

    def _trd(self,**kwargs):
        name = self._get_var("name", str, "atr",**kwargs)
        x1   = self._get_var("x1", float, "lgt",**kwargs)/2
        x2   = self._get_var("x2", float, "lgt",**kwargs)/2
        y1   = self._get_var("y1", float, "lgt",**kwargs)/2
        y2   = self._get_var("y2", float, "lgt",**kwargs)/2
        z    = self._get_var("z", float, "lgt",**kwargs)/2

        csgsolid = Trd(name, x1, x2, y1, y2, z)
        return csgsolid

    def _torus(self,**kwargs):
        name  = self._get_var("name", str, "atr",**kwargs)
        rmin  = self._get_var("rmin", float, "lgt",**kwargs)
        rmax  = self._get_var("rmax", float, "lgt",**kwargs)
        rtor  = self._get_var("rmax", float, "lgt",**kwargs)
        sphi  = self._get_var("startphi",float, "ang", **kwargs)
        dphi  = self._get_var("deltaphi", float, "ang", **kwargs)

        csgsolid = Torus(name, rmin, rmax, rtor, sphi, dphi)
        return csgsolid

    def _polyhedra(self,**kwargs):
        name     = self._get_var("name", str, "atr",**kwargs)
        sphi     = self._get_var("startphi",float, "ang", **kwargs)
        dphi     = self._get_var("deltaphi", float, "ang", **kwargs)
        nsides   = self._get_var("numsides", int, "atr", **kwargs)
        nzpl     = self._get_var("nzplanes", int, "atr", **kwargs)

        Rmin = []
        Rmax = []
        Z    = []
        for i in range(nzpl):
            rmin     = self._get_var("rmin_"+str(i), float, "lgt",**kwargs)
            rmax     = self._get_var("rmax_"+str(i), float, "lgt",**kwargs)
            z        = self._get_var("z_"+str(i), float, "lgt",**kwargs)
            Rmin.append(rmin)
            Rmax.append(rmax)
            Z.append(z)

        csgsolid = Polyhedra(name, sphi, dphi, nsides, nzpl, Z, Rmin, Rmax)
        return csgsolid

    def _xtru(self,**kwargs):
        name    = self._get_var("name", str, "atr",**kwargs)

        nzpl    = self._get_var("nzplanes", int, "atr", **kwargs)
        nvrt    = self._get_var("nverts", int, "atr", **kwargs)

        verts   = []
        zplanes = []

        for i in range(nvrt):
            x     = self._get_var("x_"+str(i), float, "lgt",**kwargs)
            y     = self._get_var("y_"+str(i), float, "lgt",**kwargs)
            vert = [x,y]
            verts.append(vert)

        for i in range(nzpl):
            zpos      = self._get_var("zPosition_"+str(i), float, "lgt",**kwargs)
            xoffs     = self._get_var("xOffset_"+str(i), float, "lgt",**kwargs)
            yoffs     = self._get_var("yOffset_"+str(i), float, "lgt",**kwargs)
            scl       = self._get_var("scalingFactor_"+str(i), float, "atr",**kwargs)

            zplane = [zpos,[xoffs,yoffs], scl]
            zplanes.append(zplane)

        csgsolid = ExtrudedSolid(name, verts, zplanes)
        return csgsolid

    def _tube(self,**kwargs):
        name  = self._get_var("name", str, "atr", **kwargs)
        rmin  = self._get_var("rmin", float, "lgt",**kwargs)
        rmax  = self._get_var("rmax", float, "lgt",**kwargs)
        sphi  = self._get_var("startphi",float, "ang", **kwargs)
        dphi  = self._get_var("deltaphi", float, "ang", **kwargs)
        z     = self._get_var("z", float, "lgt", **kwargs)/2

        csgsolid = Tubs(name, rmin, rmax, z, sphi, dphi)
        return csgsolid

    def _subtraction(self,**kwargs):
        name     = kwargs.get("name")
        first    = kwargs.get("first")
        second   = kwargs.get("second")
        pos_dict = kwargs.get("position", {})
        rot_dict = kwargs.get("rotation", {})

        try:                                   #if both inital solids are not correctly constructed this will fail
            first_solid  = self.solids[first]
            second_solid = self.solids[second]

            x_rot = self._get_var("x", float, "ang", **rot_dict)
            y_rot = self._get_var("y", float, "ang", **rot_dict)
            z_rot = self._get_var("z", float, "ang", **rot_dict)

            x_pos = self._get_var("x", float, "lgt", **pos_dict)
            y_pos = self._get_var("y", float, "lgt", **pos_dict)
            z_pos = self._get_var("z", float, "lgt", **pos_dict)

            transform = [[x_rot, y_rot, z_rot],[x_pos, y_pos, z_pos]]


            csgsolid = Subtraction(name, first_solid, second_solid, transform)

        except:
            csgsolid = None

        return csgsolid

    def _union(self,**kwargs):
        name     = kwargs.get("name")
        first    = kwargs.get("first")
        second   = kwargs.get("second")
        pos_dict = kwargs.get("position", {})
        rot_dict = kwargs.get("rotation", {})

        try:                                   #if both inital solids are not correctly constructed this will fail
            first_solid  = self.solids[first]
            second_solid = self.solids[second]

            x_rot = self._get_var("x", float, "ang", **rot_dict)
            y_rot = self._get_var("y", float, "ang", **rot_dict)
            z_rot = self._get_var("z", float, "ang", **rot_dict)

            x_pos = self._get_var("x", float, "lgt", **pos_dict)
            y_pos = self._get_var("y", float, "lgt", **pos_dict)
            z_pos = self._get_var("z", float, "lgt", **pos_dict)

            transform = [[x_rot, y_rot, z_rot],[x_pos, y_pos, z_pos]]


            csgsolid = Union(name, first_solid, second_solid, transform)

        except:
            csgsolid = None

        return csgsolid

    def _intersection(self,**kwargs):
        name     = kwargs.get("name")
        first    = kwargs.get("first")
        second   = kwargs.get("second")
        pos_dict = kwargs.get("position", {})
        rot_dict = kwargs.get("rotation", {})

        try:                                   #if both inital solids are not correctly constructed this will fail
            first_solid  = self.solids[first]
            second_solid = self.solids[second]

            x_rot = self._get_var("x", float, "ang", **rot_dict)
            y_rot = self._get_var("y", float, "ang", **rot_dict)
            z_rot = self._get_var("z", float, "ang", **rot_dict)

            x_pos = self._get_var("x", float, "lgt", **pos_dict)
            y_pos = self._get_var("y", float, "lgt", **pos_dict)
            z_pos = self._get_var("z", float, "lgt", **pos_dict)

            transform = [[x_rot, y_rot, z_rot],[x_pos, y_pos, z_pos]]


            csgsolid = Intersection(name, first_solid, second_solid, transform)

        except:
            csgsolid = None

        return csgsolid


    def _constructCSGSolid(self, solid_type, attributes):
        """
        Constructs a Pycsg Solid from the attributes of a GDML solid.

        Inputs:
          attributes: dictionary of parameters for the solid

        Returns:
          Instance of one of the solids supported by pygdml or None
          if the solid is not supported
        """
        supported_solids = {"box": self._box, "para": self._para, "tube": self._tube, "cone": self._cone, "ellipsoid": self._ellipsoid,
                            "polyhedra": self._polyhedra, "torus": self._torus, "xtru": self._xtru, "cutTube": self._cutTube,
                            "trd":self._trd, "sphere":self._sphere, "orb": self._orb, "subtraction": self._subtraction,
                             "intersection": self._intersection, "union": self._union}

        st = solid_type
        if st in supported_solids.keys():
            solid = supported_solids[st](**attributes)
            if(solid is not None):
                #print "Solid successfuly constructed: "+st+" "+attributes["name"]
                pass
            else:
                #warnings.warn("Solid construction failed: "+st+" "+attributes["name"])
                "Solid construction failed: "+st+" "+attributes["name"]
            return solid
        else:
            print "Solid "+st+" not supported, abort construction"


    def _get_var(self, varname, var_type, param_type, **kwargs):

        if(var_type == int):   #inputs are all stings so set defaults to proper type
            default = 0
        elif(var_type == float):
            default = 0.0
        elif(var_type == str):
            default = ""

        #search for the absolute value
        try:
            var = var_type(kwargs.get(varname, default))    #get attribute value if attribute is present

        except(ValueError):                                 #if attribute found, but typecasting fails, search defines to check if its referenced
            try:
                var = var_type(self.quantities[kwargs.get(varname, default)])
            except(KeyError):                               #if attribute value is not found in defined quantities, look in constants
                try:
                    var = eval(self.constants[kwargs.get(varname, default)])
                    #print varname," ",kwargs.get(varname, default)," ",var
                except(KeyError):
                    warnings.warn("Variable "+varname+" not found")
                    var = None

        #convert units where neccessary
        if var is not default:
            if("unit" in kwargs):
                uts = kwargs["unit"]
            elif("aunit" in kwargs and param_type=="ang"):
                uts = kwargs["aunit"]
            elif("lunit" in kwargs and param_type=="lgt"):
                uts = kwargs["lunit"]
            else:
                uts = "default"
        else:
            uts = "default"

        var = self._toStandUnits(var,uts)

        return var

    def _getCoordinateList(self, kwargs):
        x = self._get_var("x", float, "atr", **kwargs)
        y = self._get_var("y", float, "atr", **kwargs)
        z = self._get_var("z", float, "atr", **kwargs)

        return [x,y,z]

    def _extractNodeData(self, node):
        node_name = node.tagName

        if node.nodeType == node.ELEMENT_NODE:
            if(node_name == "volume"):
                name      = node.attributes["name"].value
                material  = node.getElementsByTagName("materialref")[0].attributes["ref"].value
                solid     = node.getElementsByTagName("solidref")[0].attributes["ref"].value
                daughters = [] #done elsewhere
                self.gdmlvols[name] = [solid, material, daughters]

            elif(node_name == "physvol"):
                volref    = node.getElementsByTagName("volumeref")[0].attributes["ref"].value
                position  = self._evalCoordRef(node, "position")
                rotation  = self._evalCoordRef(node, "rotation")
                scale     = self._evalCoordRef(node, "scale")
                mother    = node.parentNode.attributes["name"].value
                self.gdmlphvols[volref].append([mother, position, rotation, scale])

            elif(node_name == "paramvol"):
                print "Volume ", node.parentNode.attributes["name"].value, "excluded - parametrised volume" #debug
                volref  = node.getElementsByTagName("volumeref")[0].attributes["ref"].value
                self.exclude.append(volref)                                                 #TODO: include parametrised solids
                """
                ncopies   = node.attributes["ncopies"].val
                volref    = node.getElementsByTagName("volumeref")[0].attributes["ref"].value
                for in range(1, ncopies):
                position  = self._evalCoordRef(node, "position")
                rotation  = self._evalCoordRef(node, "rotation")
                mother    = node.parentNode.attributes["name"].value
                self.gdmlphvols[volref] = [mother, position, rotation]

                print volref," ",position," ", rotation #DEBUG
                """
            for child in node.childNodes:
                self._extractNodeData(child)

    def _evalCoordRef(self, node, coordstype): #TODO(aabramov): optimise fetching of parameters using self.get_var

        try:
            if(coordstype == "rotation"):
                aslist = self.rotations[node.getElementsByTagName("rotationref")[0].attributes["ref"].value] #coordinate conversion is done at reading for member dicts
            elif(coordstype == "position"):
                aslist = self.positions[node.getElementsByTagName("positionref")[0].attributes["ref"].value]
            elif(coordstype == "scale"):
                aslist = self.positions[node.getElementsByTagName("scaleref")[0].attributes["ref"].value]
            else:
                warnings.warn("Invalid coordinate type "+coordstype+". Valid types are 'position' and 'rotation'")
                aslist=None
        except(IndexError):
            try:
                if(coordstype == "position"):
                    crd = node.getElementsByTagName(coordstype)[0]
                    try:
                        uts = crd.attributes["unit"].value
                    except(KeyError):
                        uts = "default"

                    x   = self._toStandUnits(float(crd.attributes["x"].value), uts)
                    y   = self._toStandUnits(float(crd.attributes["y"].value), uts)
                    z   = self._toStandUnits(float(crd.attributes["z"].value), uts)
                    aslist = [x,y,z]

                elif(coordstype == "rotation"):
                    crd = node.getElementsByTagName(coordstype)[0]
                    try:
                        uts = crd.attributes["unit"].value
                    except(KeyError):
                        uts = "default"

                    x   = self._toStandUnits(float(crd.attributes["x"].value), uts)
                    y   = self._toStandUnits(float(crd.attributes["y"].value), uts)
                    z   = self._toStandUnits(float(crd.attributes["z"].value), uts)
                    aslist = [x,y,z]

                elif(coordstype == "scale"):
                    x = float(node.getElementsByTagName("scale")[0].attributes["x"].value)
                    y = float(node.getElementsByTagName("scale")[0].attributes["y"].value)
                    z = float(node.getElementsByTagName("scale")[0].attributes["z"].value)
                    aslist = [x,y,z]

                else:
                    warnings.warn("Warning: invalid coordinate type "+coordstype+". Valid types are 'position' and 'rotation'")
                    aslist=None

            except(IndexError):
                if (coordstype == "scale"):
                    aslist=[1.,1.,1.]
                else:
                    aslist = [0.0, 0.0, 0.0]

        return aslist

    def _toStandUnits(self, value, unit):
        #standard units are mm for length and rad for angle
        multf = {"default":1, "pm":1.e-6, "nm":1.e-3, "mum":1.e-3, "mm":1, "cm":10, "m":1.e3, "deg":2*pi/360, "rad":1}
        try:
            val = multf[unit]*value #if this fails the value is of unknown unit type
        except:
            return value
        #print val," ",unit," ",val #DEBUG

        return val

    def _buildPycsgStructure(self, name, mother, startstring="|", verbose=False):
        solidname  = self.gdmlvols[name][0]
        solid      = self.solids.get(solidname, None)
        material   = self.gdmlvols[name][1]
        daughters  = self.gdmlvols[name][2]

        phvol_prms = self.gdmlphvols.get(name, [[None,[0.0,0.0,0.0],[0.0,0.0,0.0],[1.,1.,1.], False]]) #the default is the world voume
        for i in range(len(phvol_prms)):

            mother_name = phvol_prms[i][0]
            position    = phvol_prms[i][1]
            rotation    = phvol_prms[i][2]
            scale       = phvol_prms[i][3]
            copyNr      = 0
            checkSurf   = False


            if(solid is not None):                    # TODO: fix the logic here
                if (mother_name is None):             # world volume special case, no mother
                    volume  = Volume(rotation, position, solid, name,  mother, copyNr, checkSurf, material, scale)
                    self.volumes.append(volume)

                    if(verbose):
                        print "\n==>VOLUME HIREACHY<=="
                        print '{:>4}'.format(str(len(self.volumes))), startstring+name #, " ROT=>",rotation," POS=>",position, " SCL=>",scale
                        startstring = startstring+"    |"

                    for daughter in daughters:
                        self._buildPycsgStructure(daughter, volume, startstring=startstring, verbose=verbose)


                elif(mother_name == mother.name): #only place physical volumes if they belong to that logical volume
                    volume  = Volume(rotation, position, solid, name,  mother, copyNr, checkSurf, material, scale)
                    self.volumes.append(volume)

                    if(verbose):
                        print '{:>4}'.format(str(len(self.volumes))), startstring+name #, " ROT=>",rotation," POS=>",position, " SCL=>",scale
                        startstring = startstring+"    |"

                    for daughter in daughters:
                        self._buildPycsgStructure(daughter, volume, startstring=startstring, verbose=verbose)


    def write(self, filename):
        directories = os.path.dirname(filename)
        try:
            os.makedirs(directories)
        except OSError: # they already exist
            pass
        with open(filename, 'w') as f:
            xmlString = self.doc.toprettyxml()
            f.write(xmlString)

    def add(self, rootVolume) :

        self.solidList  = []
        self.volumeList = []
        # A dictionary to store mangled material references in.
        self.mangled_references = {}

        self.imp = getDOMImplementation()
        self.doc = self.imp.createDocument(None,"gdml",None)
        # Define the root element of the document.
        self.top = self.doc.documentElement
        self.top.setAttribute('xmlns:xsi','http://www.w3.org/2001/XMLSchema-instance')
        self.top.setAttribute(' xsi:noNamespaceSchemaLocation','http://service-spi.web.cern.ch/service-spi/app/releases/GDML/schema/gdml.xsd')
#        lengthComment = self.doc.createComment(' size '+
#                                               str(rootVolume.size[0])+' '+
#                                               str(rootVolume.size[1])+' '+
#                                               str(rootVolume.size[2])+' ')
#        self.top.appendChild(lengthComment)
        self.de  = self.top.appendChild(self.doc.createElement("define"))
        self.me  = self.top.appendChild(self.doc.createElement("materials"))
        self.oe  = self.top.appendChild(self.doc.createElement("solids"))
        self.se  = self.top.appendChild(self.doc.createElement("structure"))

        # Add the size
        r = self.doc.createElement('position')
        r.setAttribute('name','PygdmlSize')
        r.setAttribute('x',str(rootVolume.size[0]))
        r.setAttribute('y',str(rootVolume.size[1]))
        r.setAttribute('z',str(rootVolume.size[2]))
        self.de.appendChild(r)

        # Add the origin
        r = self.doc.createElement('position')
        r.setAttribute('name','PygdmlOrigin')
        r.setAttribute('x',str(rootVolume.origin[0]))
        r.setAttribute('y',str(rootVolume.origin[1]))
        r.setAttribute('z',str(rootVolume.origin[2]))
        self.de.appendChild(r)

        # need to write the file in order
        self.pvfix = dict()


        self.rootVolume = rootVolume
        self.addVolumes(self.rootVolume)

        self.we.appendChild(self.wsr) # append world solidref (again order problem)

        # need to write the file in order
        for k in self.pvfix :
            for e in self.doc.getElementsByTagName("volume") :
                if e.hasAttribute('name') and e.getAttribute('name') == k :
                    for pv in self.pvfix[k] :
                        e.appendChild(pv)


        ss = self.doc.createElement('setup') # Structure setup
        ss.setAttribute('name','Default')
        ss.setAttribute('version','1.0')
        sse = self.doc.createElement('world')
        sse.setAttribute('ref',self.rootVolume.name+'_lv')
        ss.appendChild(sse)
        self.top.appendChild(ss)

    def addVolumes(self,volume):
        # print 'gdml.addVolume>',volume.name

        try :
            if self.volumeList.index(volume.name) >= 0 :
                print 'gdml.addVolume> Done this volume returning',volume.name
                return
        except ValueError :
            self.volumeList.append(volume.name)

        # recurse down adding daughter volumes
        for dv in volume.daughterVolumes:
            pv = self.addVolumes(dv)

        # get current solid
        cv = volume.currentVolume
        current_material = cv.material

        # If material is just a string then just use it directly.
        if isinstance(current_material, basestring):
            materialReference = current_material
        # Else if a material instance:
        elif isinstance(current_material, (Material,
                                           MaterialReference)):
            self.addMaterial(current_material)
            materialReference = self.mangled_references[current_material]

        # add solid to solidElement
        self.addSolid(cv)

        if volume.motherVolume == None :
            self.we = self.doc.createElement('volume')
            self.we.setAttribute('name',volume.name+'_lv')
            mr = self.doc.createElement('materialref')
            mr.setAttribute('ref',materialReference)
            self.we.appendChild(mr)

            sr = self.doc.createElement('solidref')
            sr.setAttribute('ref',cv.name)
            # self.we.appendChild(sr)
            self.wsr = sr # order issue
            self.se.appendChild(self.we)
        else :
            # logical volume
            lv = self.doc.createElement('volume')
            lv.setAttribute('name',volume.name+'_lv')
            mr = self.doc.createElement('materialref')
            mr.setAttribute('ref',materialReference)
            lv.appendChild(mr)
            sr = self.doc.createElement('solidref')
            sr.setAttribute('ref',cv.name)
            lv.appendChild(sr)
            self.se.appendChild(lv)

            # physical volume
            pv = self.doc.createElement('physvol')
            pv.setAttribute('name',volume.name+'_pv')
            vr = self.doc.createElement('volumeref')
            vr.setAttribute('ref',volume.name+'_lv')
            pv.appendChild(vr)


            # phys vol translation
            tlatee = self.doc.createElement('position')
            tlatee.setAttribute('name',volume.name+'_pos')
            tlatee.setAttribute('x',str(volume.tlate[0]))
            tlatee.setAttribute('y',str(volume.tlate[1]))
            tlatee.setAttribute('z',str(volume.tlate[2]))
            pv.appendChild(tlatee)

            # phys vol rotation
            rote   = self.doc.createElement('rotation')
            rote.setAttribute('name',volume.name+'_rot')
            rote.setAttribute('x',str(volume.rot[0]))
            rote.setAttribute('y',str(volume.rot[1]))
            rote.setAttribute('z',str(volume.rot[2]))
            pv.appendChild(rote)

            # phys vol scale
            tscae = self.doc.createElement('scale')
            tscae.setAttribute('name',volume.name+'_sca')
            tscae.setAttribute('x',str(volume.scale[0]))
            tscae.setAttribute('y',str(volume.scale[1]))
            tscae.setAttribute('z',str(volume.scale[2]))
            pv.appendChild(tscae)

            try :
                self.pvfix[volume.motherVolume.name+'_lv'].append(pv)
            except KeyError :
                self.pvfix[volume.motherVolume.name+'_lv'] = []
                self.pvfix[volume.motherVolume.name + '_lv'].append(pv)

            # if the order didn't matter (but instead lines above)
            #for e in self.doc.getElementsByTagName("volume") :
            #    if e.hasAttribute('name') and e.getAttribute('name') == volume.motherVolume.name+'_lv' :
            #        e.appendChild(pv)

    def addSolid(self, cs):

        # print cs.name,self.solidList
        try :
            if self.solidList.index(cs.name) >= 0 :
                print 'gdml.addSolid> Done this solid',cs.name,self.solidList
                return
        except ValueError :
            self.solidList.append(cs.name)

        # print 'gdml.addSolid>',cs.name, cs.type
        if cs.type == 'box' :
            oe = self.doc.createElement('box')
            oe.setAttribute('name',cs.name)
            oe.setAttribute('x',repr(2*cs.pX))
            oe.setAttribute('y',repr(2*cs.pY))
            oe.setAttribute('z',repr(2*cs.pZ))
            self.oe.appendChild(oe)
        elif cs.type == 'tubs' :
            oe = self.doc.createElement("tube")
            oe.setAttribute('name',cs.name)
            oe.setAttribute('rmin',repr(cs.pRMin))
            oe.setAttribute('rmax',repr(cs.pRMax))
            oe.setAttribute('z',   repr(2*cs.pDz))
            oe.setAttribute('startphi',repr(cs.pSPhi))
            oe.setAttribute('deltaphi',repr(cs.pDPhi))
            self.oe.appendChild(oe)
        elif cs.type == 'cuttubs' :
            oe = self.doc.createElement("cutTube")
            oe.setAttribute('name',cs.name)
            oe.setAttribute('rmin',repr(cs.pRMin))
            oe.setAttribute('rmax',repr(cs.pRMax))
            oe.setAttribute('z',   repr(2*cs.pDz))
            oe.setAttribute('startphi',repr(cs.pSPhi))
            oe.setAttribute('deltaphi',repr(cs.pDPhi))
            oe.setAttribute('lowX',repr(cs.pLowNorm[0]))
            oe.setAttribute('lowY',repr(cs.pLowNorm[1]))
            oe.setAttribute('lowZ',repr(cs.pLowNorm[2]))
            oe.setAttribute('highX',repr(cs.pHighNorm[0]))
            oe.setAttribute('highY',repr(cs.pHighNorm[1]))
            oe.setAttribute('highZ',repr(cs.pHighNorm[2]))
            self.oe.appendChild(oe)
        elif cs.type == "ellipticaltube":
            oe = self.doc.createElement("eltube")
            oe.setAttribute("name", repr(cs.name))
            oe.setAttribute("dx", repr(cs.pDx))
            oe.setAttribute("dy", repr(cs.pDy))
            oe.setAttribute("dz", repr(cs.pDz))
            self.oe.appendChild(oe)
        elif cs.type == 'orb' :
            oe = self.doc.createElement('orb')
            oe.setAttribute('name',cs.name)
            oe.setAttribute('r',repr(cs.pRMax))
            self.oe.appendChild(oe)
        elif cs.type == 'torus' :
            oe = self.doc.createElement('torus')
            oe.setAttribute('name',cs.name)
            oe.setAttribute('rmin',repr(cs.pRmin))
            oe.setAttribute('rmax',repr(cs.pRmax))
            oe.setAttribute('rtor',repr(cs.pRtor))
            oe.setAttribute('deltaphi',repr(cs.pDPhi))
            oe.setAttribute('startphi',repr(cs.pSPhi))
            self.oe.appendChild(oe)
        elif cs.type == 'cons' :
            oe = self.doc.createElement('cone')
            oe.setAttribute('name',cs.name)
            oe.setAttribute('rmin1',repr(cs.pRmin1))
            oe.setAttribute('rmax1',repr(cs.pRmax1))
            oe.setAttribute('rmin2',repr(cs.pRmin2))
            oe.setAttribute('rmax2',repr(cs.pRmax2))
            oe.setAttribute('z',repr(2 * cs.pDz))
            oe.setAttribute('startphi',repr(cs.pSPhi))
            oe.setAttribute('deltaphi',repr(cs.pDPhi))
            self.oe.appendChild(oe)
        elif cs.type == 'para' :
            oe = self.doc.createElement('para')
            oe.setAttribute('name',cs.name)
            oe.setAttribute('x',repr(cs.pX))
            oe.setAttribute('y',repr(cs.pY))
            oe.setAttribute('z',repr(cs.pZ))
            oe.setAttribute('alpha',repr(cs.pAlpha))
            oe.setAttribute('theta',repr(cs.pTheta))
            oe.setAttribute('phi',repr(cs.pPhi))
            self.oe.appendChild(oe)
        elif cs.type == 'sphere' :
            oe = self.doc.createElement('sphere')
            oe.setAttribute('name',cs.name)
            oe.setAttribute('rmin',repr(cs.pRmin))
            oe.setAttribute('rmax',repr(cs.pRmax))
            oe.setAttribute('deltaphi',repr(cs.pDPhi))
            oe.setAttribute('startphi',repr(cs.pSPhi))
            oe.setAttribute('starttheta',repr(cs.pSTheta))
            oe.setAttribute('deltatheta',repr(cs.pDTheta))
            self.oe.appendChild(oe)
        elif cs.type == 'trd' :
            oe = self.doc.createElement("trd")
            oe.setAttribute('name',cs.name)
            oe.setAttribute('x1',repr(2*cs.pX1))
            oe.setAttribute('x2',repr(2*cs.pX2))
            oe.setAttribute('y1',repr(2*cs.pY1))
            oe.setAttribute('y2',repr(2*cs.pY2))
            oe.setAttribute('z',repr(2*cs.pZ))
            self.oe.appendChild(oe)
        elif cs.type == 'trap' :
            oe = self.doc.createElement("trap")
            oe.setAttribute('name',cs.name)
            oe.setAttribute('z',repr(2*cs.pDz))
            oe.setAttribute('theta', repr(cs.pTheta))
            oe.setAttribute('phi',repr(cs.pDPhi))
            oe.setAttribute('y1',repr(2*cs.pDy1))
            oe.setAttribute('x1',repr(2*cs.pDx1))
            oe.setAttribute('x2',repr(2*cs.pDx2))
            oe.setAttribute('alpha1',repr(cs.pAlp1))
            oe.setAttribute('y2',repr(2*cs.pDy2))
            oe.setAttribute('x3',repr(2*cs.pDx3))
            oe.setAttribute('x4',repr(2*cs.pDx4))
            oe.setAttribute('alpha2',repr(cs.pAlp1))
            self.oe.appendChild(oe)
        elif cs.type == 'extrudedsolid' :
            oe = self.doc.createElement('xtru')
            oe.setAttribute('name',cs.name)
            for v in cs.vertices :
                vce = self.doc.createElement('twoDimVertex')
                vce.setAttribute('x',repr(v[0]))
                vce.setAttribute('y',repr(v[1]))
                oe.appendChild(vce)
            for i in range(len(cs.zpos)) :
                vce = self.doc.createElement('section')
                vce.setAttribute('zOrder',repr(i))
                vce.setAttribute('zPosition',repr(cs.zpos[i]))
                vce.setAttribute('xOffset',repr(cs.x_offs[i]))
                vce.setAttribute('yOffset',repr(cs.y_offs[i]))
                vce.setAttribute('scalingFactor',repr(cs.scale[i]))
                oe.appendChild(vce)
            self.oe.appendChild(oe)
        elif cs.type == 'boolu' :
            # Add the lower level solids first
            self.addSolid(cs.obj1)
            self.addSolid(cs.obj2)

            oe  = self.doc.createElement('union')
            oe.setAttribute('name',cs.name)
            cfe = self.doc.createElement('first')
            cfe.setAttribute('ref',cs.obj1.name)
            oe.appendChild(cfe)

            cse = self.doc.createElement('second')
            cse.setAttribute('ref',cs.obj2.name)
            oe.appendChild(cse)

            csce = self.doc.createElement('positionref')
            csce.setAttribute('ref',cs.name+'_sol2_pos')
            oe.appendChild(csce)

            csce1 = self.doc.createElement('rotationref')
            csce1.setAttribute('ref', cs.name + '_sol2_rot')
            oe.appendChild(csce1)

            p = self.doc.createElement('position')
            p.setAttribute('name',cs.name+'_sol2_pos')
            p.setAttribute('x',repr(cs.tra2[1][0]))
            p.setAttribute('y',repr(cs.tra2[1][1]))
            p.setAttribute('z',repr(cs.tra2[1][2]))
            self.de.appendChild(p)

            r = self.doc.createElement('rotation')
            r.setAttribute('name',cs.name + '_sol2_rot')
            r.setAttribute('x', repr(cs.tra2[0][0]))
            r.setAttribute('y', repr(cs.tra2[0][1]))
            r.setAttribute('z', repr(cs.tra2[0][2]))
            self.de.appendChild(r)

            self.oe.appendChild(oe)

        elif cs.type == 'booli' :
            # Add the lower level solids first
            self.addSolid(cs.obj1)
            self.addSolid(cs.obj2)

            oe  = self.doc.createElement('intersection')
            oe.setAttribute('name',cs.name)
            cfe = self.doc.createElement('first')
            cfe.setAttribute('ref',cs.obj1.name)
            oe.appendChild(cfe)

            cse = self.doc.createElement('second')
            cse.setAttribute('ref',cs.obj2.name)
            oe.appendChild(cse)

            csce = self.doc.createElement('positionref')
            csce.setAttribute('ref',cs.name+'_sol2_pos')
            oe.appendChild(csce)

            csce1 = self.doc.createElement('rotationref')
            csce1.setAttribute('ref', cs.name + '_sol2_rot')
            oe.appendChild(csce1)

            p = self.doc.createElement('position')
            p.setAttribute('name',cs.name+'_sol2_pos')
            p.setAttribute('x',repr(cs.tra2[1][0]))
            p.setAttribute('y',repr(cs.tra2[1][1]))
            p.setAttribute('z',repr(cs.tra2[1][2]))
            self.de.appendChild(p)

            r = self.doc.createElement('rotation')
            r.setAttribute('name',cs.name + '_sol2_rot')
            r.setAttribute('x', repr(cs.tra2[0][0]))
            r.setAttribute('y', repr(cs.tra2[0][1]))
            r.setAttribute('z', repr(cs.tra2[0][2]))
            self.de.appendChild(r)

            self.oe.appendChild(oe)
        elif cs.type == 'bools' :
            # Add the lower level solids first
            self.addSolid(cs.obj1)
            self.addSolid(cs.obj2)

            oe  = self.doc.createElement('subtraction')
            oe.setAttribute('name',cs.name)
            cfe = self.doc.createElement('first')
            cfe.setAttribute('ref',cs.obj1.name)
            oe.appendChild(cfe)

            cse = self.doc.createElement('second')
            cse.setAttribute('ref',cs.obj2.name)
            oe.appendChild(cse)

            csce = self.doc.createElement('positionref')
            csce.setAttribute('ref',cs.name+'_sol2_pos')
            oe.appendChild(csce)

            csce1 = self.doc.createElement('rotationref')
            csce1.setAttribute('ref', cs.name + '_sol2_rot')
            oe.appendChild(csce1)

            p = self.doc.createElement('position')
            p.setAttribute('name',cs.name+'_sol2_pos')
            p.setAttribute('x',repr(cs.tra2[1][0]))
            p.setAttribute('y',repr(cs.tra2[1][1]))
            p.setAttribute('z',repr(cs.tra2[1][2]))
            self.de.appendChild(p)

            r = self.doc.createElement('rotation')
            r.setAttribute('name',cs.name + '_sol2_rot')
            r.setAttribute('x', repr(cs.tra2[0][0]))
            r.setAttribute('y', repr(cs.tra2[0][1]))
            r.setAttribute('z', repr(cs.tra2[0][2]))
            self.de.appendChild(r)

            self.oe.appendChild(oe)

    def addMaterial(self, material):

        # Recursively get all the components making up the material
        components = flatten_material(material)
        # Reverse so that most atomic component comes first.  This
        # ensures the gdml will be written in the correct order.
        components.reverse()
        # Delete any duplicate entries whilst preserving order.
        components = _OrderedDict.fromkeys(components).keys()

        # Get the definitions here that have not already been added to
        # the document, and also ignore Material and ElementReferences
        # as these are by definition built into the application in question.
        new_defns = [defn for defn in components
                           if (defn not in self.mangled_references
                               and not isinstance(defn, (MaterialReference,
                                                         ElementReference)))]

        # Generate mangled names for these new definitions.
        # Except don't do this for Material/Element references.
        new_names = [_mangled(defn.name) for defn in components
                     if defn not in self.mangled_references
                     and not isinstance(defn, (MaterialReference,
                                               ElementReference))]

        # Add the built-in references to the dictionary as well, but
        # don't mangle.
        built_in_names = [(component, component.name)
                          for component in components
                          if isinstance(component, (MaterialReference,
                                                    ElementReference))]

        # Add definitions and mangled names to the dictionary.
        self.mangled_references.update(dict(zip(new_defns,
                                                new_names)))
        self.mangled_references.update(dict(built_in_names))

        # Loop over and add only  the unique isotopes that haven't
        # already been written.
        for isotope in filter(lambda x: isinstance(x, Isotope), new_defns):
            isotope_reference = self.mangled_references[isotope]
            isotope_ele = self.doc.createElement("isotope")
            isotope_ele.setAttribute("name", isotope_reference)
            isotope_ele.setAttribute("N", str(isotope.mass_number))
            isotope_ele.setAttribute("Z", str(isotope.atomic_number))

            isotope_atom_ele = self.doc.createElement("atom")
            isotope_atom_ele.setAttribute("unit", "g/mole")
            isotope_atom_ele.setAttribute("value", str(isotope.molar_mass))

            isotope_ele.appendChild(isotope_atom_ele)
            self.me.appendChild(isotope_ele)

        # Loop over and add only the unique  elements that haven't
        # already been written.
        for element in filter(lambda x: isinstance(x, Element), new_defns):
            element_name = self.mangled_references[element]
            element_ele = self.doc.createElement("element")
            element_ele.setAttribute("name", element_name)

            component_fractions =  [component.fraction
                                    for component in element]
            mangled_component_names = [self.mangled_references[component]
                                       for component in element]

            for name, fraction in zip(mangled_component_names, component_fractions):
                fraction_ele = self.doc.createElement("fraction")
                fraction_ele.setAttribute("n", str(fraction))
                fraction_ele.setAttribute("ref", name)
                element_ele.appendChild(fraction_ele)

            self.me.appendChild(element_ele)

        # Add materials
        for material in filter(lambda x: isinstance(x, Material), new_defns):
            material_name = self.mangled_references[material]
            # Add material tag with attributes.
            material_ele = self.doc.createElement("material")
            material_ele.setAttribute("name", material_name)
            material_ele.setAttribute("state", str(material.state))

            # Get the temperature and add it.
            temperature = self.doc.createElement("T")
            temperature.setAttribute("unit", "K")
            temperature.setAttribute("value", str(material.temperature))
            material_ele.appendChild(temperature)

            # Get the density and add it.
            density = self.doc.createElement("D")
            density.setAttribute("unit","g/cm3")
            density.setAttribute("value", str(material.density))
            material_ele.appendChild(density)

            # If the material is a gas then add the pressure,
            # otherwise don't bother.
            if material.state == "gas":
                pressure = self.doc.createElement("P")
                pressure.setAttribute("unit","pascal")
                pressure.setAttribute("value", str(material.pressure))
                material_ele.appendChild(pressure)

            # Get the component fractions and the mangled names for
            # the definitions.
            component_fractions =  [component.fraction
                                    for component in material]
            mangled_component_names = [self.mangled_references[component]
                                       for component in material]

            for name, fraction in zip(mangled_component_names,
                                      component_fractions):
                fraction_ele = self.doc.createElement("fraction")
                fraction_ele.setAttribute("n", str(fraction))
                fraction_ele.setAttribute("ref", name)
                material_ele.appendChild(fraction_ele)

            # Get the properties and write them.
            # If property is a single property then write it
            if isinstance(material.properties, MaterialProperty):
                property_ele = self.doc.createElement(material.properties.variable)
                for key, entry in material.properties.attributes.iteritems():
                    property_ele.setAttribute(str(key), str(entry))
            # If it's a list of properties then add them each.
            elif isinstance(material.properties, list):
                for material_property in material.properties:
                    property_ele = self.doc.createElement(material_property.variable)
                    for key, entry in material_property.attributes.iteritems():
                        property_ele.setAttribute(str(key), str(value))
                    material_ele.append(property_ele)
            self.me.appendChild(material_ele)


def _mangled(name):
    """
    Mangle the name and also do a bit of extra formatting so that the
    user-provided name is still easy to read.
    """
    suffix = str(_uuid4())
    suffix = suffix.replace("-","")
    name = name + "_" + suffix
    return name
