import numpy as _np

from pycsgMod.core import CSG as _CSG
from pycsgMod.geom import Vertex as _Vertex
from pycsgMod.geom import Vector as _Vector
from pycsgMod.geom import Polygon as _Polygon

import math as _math
from math import cos as _cos
from math import sin as _sin
from math import acos as _acos
from math import asin as _asin

from transformation import rad2deg, deg2rad, tbxyz, reverse
from volume import Volume
from vtkviewer      import *

"""
Geant4 supported CSG solids:
  1. Box................Rectangular box............................*Complete*
  2. Tubs...............Cylindrical section or tube................*Complete*
  3. CutTubs............Cylindrical cut section or cut tube........*Complete*
  4. Cons...............Cone or conical section....................*Complete
  5. Para...............Parallelepiped.............................*Complete
  6. Trd................Trapezoid..................................*Complete
  7. Trap...............Generic trapezoid..........................Constructor
  8. Sphere.............Sphere or spherical shell section..........*Complete*
  9. Orb................Full solid sphere..........................*Complete*
 10. Torus..............Torus or toroidal section..................*Complete*
 11. Polycone(PCON).....Polycone or polycone section...............*Complete
 12. Polyhedra(PGON)....Polyhedra or polyhedra section.............Constructor
 13. Elliptical tube....Tube with eliptical cross-section..........*Complete*
 14. Ellipsoid..........General ellipsoid..........................*Complete*
 15. Elliptical cone....Cone with elliptical cross-section.........*Complete*
 16. Parraboloid........Solid wit parabolic profile and cuts.......Constructor
 17. Hype...............Tube with hyberbolic profile...............*Complete*
 18. Tet................Tetrahedra.................................*Complete
 19. ExtrudedSolid......Extrusion polygon..........................*Complete
 20. TwistedBox.........Box with twist along one axis..............*Complete
 21. TwistedTrap........Trapezoid with twist along one axis........Constructor
 22. TwistedTrd.........Twisted trapezoid with x,y varying along z.
 23. GenericTrap........Arbitrary trapezoid with up to 8 vertices..
 24. TwistedTubs........Tube section with a twist along one axis...
"""

class SolidBase(object):
    def __repr__(self):
        return ("<{}: name=\"{}\">".format(type(self).__name__, self.name))

    def view(self, rotation=[0, 0, 0], translation=[0, 0, 0]):
        w = Box("world", 1000, 1000, 1000)
        world_volume = Volume([0, 0, 0], [0, 0, 0], w,
                              "world-volume", None,
                              1, False, "G4_NITROUS_OXIDE")
        self_volume = Volume(rotation,
                             translation,
                             self,
                             self.name,
                             world_volume,
                             1,
                             False,
                             "G4_Cu")

        world_volume.setClip()
        mesh = world_volume.pycsgmesh()
        viewer = VtkViewer()
        viewer.addSource(mesh)
        viewer.view()

class Boolean(object):
    def dump(self):
        print (self.name, type(self))
        if isinstance(self.obj1, Boolean):
            self.obj1.dump()
        else:
            print (self.obj1.name, type(self.obj1))
        if isinstance(self.obj2, Boolean):
            self.obj2.dump()
        else:
            print (self.obj2.name, type(self.obj2))

    def view_debug(self, translation=[0, 0, 0]):
        solid1 = self.obj1
        solid2 = self.obj2
        tra2 = self.tra2
        rot2 = reverse(tra2[0])

        world_box = Box("world", 10000, 10000, 10000)
        world_volume = Volume([0, 0, 0], [0, 0, 0], world_box,
                              "world-volume", None,
                              1, False, "G4_NITROUS_OXIDE")
        volume1 = Volume([0, 0, 0], [0, 0, 0], solid1,
                         solid1.name, world_volume,
                         1, False, "G4_NITROUS_OXIDE")
        volume2 = Volume(rot2, tra2[1], solid2,
                         solid2.name, world_volume,
                         1, False, "G4_NITROUS_OXIDE")
        if translation == [0, 0, 0]:
            world_volume.setClip()
        mesh = world_volume.pycsgmesh()
        viewer = VtkViewer()
        viewer.addSource(mesh)
        viewer.view()

class Plane(SolidBase) : # point on plane is on z-axis
    def __init__(self, name, normal, dist, zlength=10000):
        self.name   = name
        self.normal = _Vector(normal).unit()
        self.dist   = dist
        self.pDz    = zlength

    def pycsgmesh(self):
        d = self.pDz
        c = _CSG.cube(radius=[10*d,10*d,d])

        dp = self.normal.dot(_Vector(0,0,1))

        if dp != 1 and dp != -1:
            cp = self.normal.cross(_Vector(0,0,1))
            an = _acos(dp)
            an = an/_math.pi*180.
            c.rotate(cp,an)

        c.translate(_Vector([0, 0, self.dist+d/dp]))

        self.mesh = c
        return self.mesh

class Wedge(SolidBase) :
    def __init__(self, name, pRMax = 1000, pSPhi=0, pDPhi=1.5, halfzlength=10000):
        self.name  = name
        self.pRMax = pRMax
        self.pSPhi = pSPhi
        self.pDPhi = pDPhi
        self.pDz   = halfzlength

        self.nslice = 16

    def pycsgmesh(self):
        d = self.pDz

        phi = _np.linspace(self.pSPhi, self.pDPhi, self.nslice)
        x  = self.pRMax*_np.cos(phi)
        y  = self.pRMax*_np.sin(phi)

        polygons = []

        vZero1 = _Vertex(_Vector(0,0,-d),None)
        vZero2 = _Vertex(_Vector(0,0, d),None)

        p1     = [ _Vertex(_Vector(x[i],y[i],-d),None)  for i in range(0,len(x))]
        p2     = [ _Vertex(_Vector(x[i],y[i], d),None)  for i in range(0,len(x))]


        for i in range(0,len(x)) :
            if i != len(x)-1 :
                # top triangle
                polygons.append(_Polygon([vZero2,p2[i],p2[i+1]]))
                # bottom triangle
                polygons.append(_Polygon([vZero1,p1[i],p1[i+1]]))
                # end square
                polygons.append(_Polygon([p1[i],p1[i+1],p2[i+1],p2[i]]))

        # first end face
        polygons.append(_Polygon([vZero1,p1[0],p2[0], vZero2]))

        # last end face
        polygons.append(_Polygon([vZero1,vZero2,p2[-1],p1[-1]]))

        self.mesh = _CSG.fromPolygons(polygons)

        return self.mesh


class Box(SolidBase) :
    """
    Constructs a box.

    Inputs:
        name:   string, name of the volume
        pX:     float, half-length along x
        pY:     float, half-length along y
        pZ:     float, half-length along z
    """
    def __init__(self, name, pX, pY, pZ) :
        self.type = "box"
        self.name = name
        self.pX = pX
        self.pY = pY
        self.pZ = pZ

    def __repr__(self):
        return ("Box \"{self.name}\": {self.pX} {self.pY}"
                " {self.pZ}".format(self=self))

    def pycsgmesh(self) :
        self.mesh = _CSG.cube(center=[0,0,0], radius=[self.pX,self.pY,self.pZ])
        return self.mesh


class Tubs(SolidBase) :
    """
    Constructs a cylindrical section.

    Inputs:
        name:   string, name of the volume
        pRMin:  float, inner radius
        pRMax:  float, outer radius
        pDz:    float, half-length along z
        pSPhi:  float, starting phi angle
        pDPhi:  float, angle of segment in radians
    """
    def __init__(self, name, pRMin, pRMax, pDz, pSPhi, pDPhi) :
        self.type  = 'tubs'
        self.name  = name
        self.pRMin = pRMin
        self.pRMax = pRMax
        self.pDz   = pDz
        self.pSPhi = pSPhi
        self.pDPhi = pDPhi

    def pycsgmesh(self):
        # set dimensions of wedge to intersect with that are much
        # larger than the dimensions of the solid
        wzlength = 3 * self.pDz
        wrmax    = 3 * self.pRMax

        sOuter = _CSG.cylinder(start=[0, 0, -self.pDz],
                               end=[0, 0, self.pDz],
                               radius=self.pRMax)
        if self.pDPhi == 2*_np.pi :
            pWedge = Wedge("wedge_temp",
                           wrmax,
                           self.pSPhi,
                           self.pSPhi + self.pDPhi - 0.0001,
                           wzlength).pycsgmesh()
        else :
            pWedge = Wedge("wedge_temp",
                           wrmax,
                           self.pSPhi,
                           self.pSPhi + self.pDPhi,
                           wzlength).pycsgmesh()

        # If a solid cylinder then just return the primitive CSG solid.
        if not self.pRMin and self.pSPhi == 0.0 and self.pDPhi == 2*_np.pi:
            return sOuter
        if(self.pRMin):
            sInner = _CSG.cylinder(start=[0, 0, -self.pDz],
                                   end=[0, 0, self.pDz],
                                   radius=self.pRMin)
            self.mesh = sOuter.subtract(sInner).subtract(pWedge.inverse())
        else:
            self.mesh = sOuter.subtract(pWedge.inverse())

        return self.mesh

    def __repr__(self):
        out_string = ("Tubs \"{self.name}\": r_max={self.pRMax}"
                      ", z={self.pDz}".format(self=self))
        phi_string = ""
        rmin_string = ""
        if self.pRMin > 0.0:
            rmin_string = " r_min={self.pRMin}".format(self=self)
        if self.pSPhi != 0.0 or self.pDPhi != 2*_math.pi:
            phi_string = (" phi_s={self.pSPhi}, "
                          "phi_d={self.pDPhi}".format(self=self))

        return out_string + rmin_string + phi_string


class CutTubs(SolidBase) :
    """
    Constructs a cylindrical section with cuts.

    Inputs:
        name:      string, name of the volume
        pRMin:     float, inner radius
        pRMax:     float, outer radius
        pDz:       float, half-length along z
        pSPhi:     float, starting phi angle
        pDPhi:     float, angle of segment in radians
        pLowNorm:  list,  normal vector of the cut plane at -pDz
        pHighNorm: list, normal vector of the cut plane at +pDz
    """
    def __init__(self, name, pRMin, pRMax, pDz, pSPhi, pDPhi, pLowNorm, pHighNorm) :
        self.type      = 'cuttubs'
        self.name      = name
        self.pRMin     = pRMin
        self.pRMax     = pRMax
        self.pDz       = pDz
        self.pSPhi     = pSPhi
        self.pDPhi     = pDPhi
        self.pLowNorm  = pLowNorm
        self.pHighNorm = pHighNorm

    def pycsgmesh(self):
        zlength = 3*self.pDz #make the dimensions of the semi-infinite plane large enough

        t =     Tubs("tubs_temp",self.pRMin, self.pRMax, 2*self.pDz, self.pSPhi, self.pDPhi).pycsgmesh()
        pHigh = Plane("pHigh_temp",self.pHighNorm,self.pDz, zlength).pycsgmesh()
        pLow  = Plane("pLow_temp" ,self.pLowNorm,-self.pDz, zlength).pycsgmesh()
        self.mesh = t.subtract(pHigh).subtract(pLow)
        return self.mesh

class Cons(SolidBase) :
    """
    Constructs a conical section.

    Inputs:
        name:      string, name of the volume
        pRMin1:    float, inner radius at -pDz
        pRMax1:    float, outer radius at -pDz
        pRMin2:    float, inner radius at +pDZ
        pRMax2:    float, outer radius at +pDz
        pDz:       float, half-length along z
        pSPhi:     float, starting phi angle
        pDPhi:     float, angle of segment in radians
    """

    def __init__(self, name, pRmin1, pRmax1, pRmin2, pRmax2, pDz, pSPhi, pDPhi) :
        self.name = name
        self.type   = 'cons'
        self.pRmin1 = pRmin1
        self.pRmax1 = pRmax1
        self.pRmin2 = pRmin2
        self.pRmax2 = pRmax2
        self.pDz    = pDz
        self.pSPhi  = pSPhi
        self.pDPhi  = pDPhi

    def pycsgmesh(self):

        if self.pRmax1 < self.pRmax2:
            R1 = self.pRmax2 #Cone with tip pointing towards -z
            r1 = self.pRmin2
            R2 = self.pRmax1
            r2 = self.pRmin1
            factor = -1

        else:
            R1 = self.pRmax1 #Cone with tip pointing towards +z
            r1 = self.pRmin1
            R2 = self.pRmax2
            r2 = self.pRmin2
            factor = 1

        h  = 2*self.pDz
        H1 = float(R2*h)/float(R1-R2)

        try:                              #Avoid crash when both inner radii are 0
            H2 = float(r2*h)/float(r1-r2)
        except ZeroDivisionError:
            H2 = 0

        h1 = factor*(h + H1)
        h2 = factor*(h + H2)

        sOuter    = _CSG.cone(start=[0,0,-factor*self.pDz], end=[0,0, h1-factor*self.pDz], radius=R1)

        wrmax    = 3*(self.pRmax1+self.pRmax2) #ensure radius for intersection wedge is much bigger than solid
        wzlength = 3*self.pDz

        if self.pDPhi != 2*_np.pi:
            pWedge    = Wedge("wedge_temp",wrmax, self.pSPhi, self.pSPhi+self.pDPhi, wzlength).pycsgmesh()
        else:
            pWedge    = _CSG.cylinder(start=[0,0,-self.pDz*5], end=[0,0,self.pDz*5], radius=R1*5) #factor 5 is just to ensure wedge mesh is much bigger than cone solid

        pTopCut   = Plane("pTopCut_temp",_Vector(0,0,1),self.pDz, wzlength).pycsgmesh()
        pBotCut   = Plane("pBotCut_temp",_Vector(0,0,-1),-self.pDz, wzlength).pycsgmesh()

        if H2:
            sInner    = _CSG.cone(start=[0,0,-factor*self.pDz], end=[0,0, h2-factor*self.pDz], radius=r1)
            self.mesh = sOuter.subtract(sInner).intersect(pWedge).subtract(pBotCut).subtract(pTopCut)
        else:
            self.mesh = sOuter.intersect(pWedge).subtract(pBotCut).subtract(pTopCut)


        return self.mesh


class Orb(SolidBase):
    """
    Constructs a solid sphere.

    Inputs:
        name:     string, name of the volume
        pRMax:    float, outer radius
    """
    def __init__(self, name, pRMax):
        self.type = 'orb'
        self.name = name
        self.pRMax = pRMax

    def pycsgmesh(self):
        self.mesh = _CSG.sphere(center=[0,0,0], radius=self.pRMax)
        return self.mesh

class Para(SolidBase) :
    """
    Constructs a parallelepiped.

    Inputs:
        name:   string, name of the volume
        pX:     float, half-length along x
        pY:     float, half-length along y
        pZ:     float, half-length along z
        pAlpha: float, angle formed by the y axis and the plane joining the centres of the faces parallel tothe z-x plane at -dy and +dy
        pTheta: float, polar angle of the line joining the centres of the faces at -dz and +dz in z
        pPhi:   float, azimuthal angle of the line joining the centres of the faces at -dx and +dz in z
    """
    def __init__(self, name, pDx, pDy, pDz, pAlpha, pTheta, pPhi) :
        self.type     = 'para'
        self.name   = name
        self.pX     = pDx
        self.pY     = pDy
        self.pZ     = pDz
        self.pAlpha = pAlpha
        self.pTheta = pTheta
        self.pPhi   = pPhi
        self.dx_y   = self.pY*_sin(self.pAlpha)  #changes sign as the y component
        self.dx_z   = +self.pZ*_sin(self.pTheta) #changes sign as the z component
        self.dy     = self.pZ*_sin(self.pPhi)
        self.dz     = self.pZ-self.pZ*_cos(pPhi)

    def pycsgmesh(self) :
        self.mesh  = _CSG.fromPolygons([_Polygon([_Vertex(_Vector(-self.pX-self.dx_y-self.dx_z,-self.pY-self.dy,-self.pZ+self.dz), None),
                                                  _Vertex(_Vector(-self.pX+self.dx_y-self.dx_z, self.pY-self.dy,-self.pZ+self.dz), None),
                                                  _Vertex(_Vector( self.pX+self.dx_y-self.dx_z, self.pY-self.dy,-self.pZ+self.dz), None),
                                                  _Vertex(_Vector( self.pX-self.dx_y-self.dx_z,-self.pY-self.dy,-self.pZ+self.dz), None)]),
                                        _Polygon([_Vertex(_Vector(-self.pX-self.dx_y+self.dx_z,-self.pY+self.dy, self.pZ-self.dz), None),
                                                  _Vertex(_Vector( self.pX-self.dx_y+self.dx_z,-self.pY+self.dy, self.pZ-self.dz), None),
                                                  _Vertex(_Vector( self.pX+self.dx_y+self.dx_z, self.pY+self.dy, self.pZ-self.dz), None),
                                                  _Vertex(_Vector(-self.pX+self.dx_y+self.dx_z, self.pY+self.dy, self.pZ-self.dz), None)]),
                                        _Polygon([_Vertex(_Vector(-self.pX-self.dx_y-self.dx_z,-self.pY-self.dy,-self.pZ+self.dz), None),
                                                  _Vertex(_Vector( self.pX-self.dx_y-self.dx_z,-self.pY-self.dy,-self.pZ+self.dz), None),
                                                  _Vertex(_Vector( self.pX-self.dx_y+self.dx_z,-self.pY+self.dy, self.pZ-self.dz), None),
                                                  _Vertex(_Vector(-self.pX-self.dx_y+self.dx_z,-self.pY+self.dy, self.pZ-self.dz), None)]),
                                        _Polygon([_Vertex(_Vector(-self.pX+self.dx_y-self.dx_z, self.pY-self.dy,-self.pZ+self.dz), None),
                                                  _Vertex(_Vector(-self.pX+self.dx_y+self.dx_z, self.pY+self.dy, self.pZ-self.dz), None),
                                                  _Vertex(_Vector( self.pX+self.dx_y+self.dx_z, self.pY+self.dy, self.pZ-self.dz), None),
                                                  _Vertex(_Vector( self.pX+self.dx_y-self.dx_z, self.pY-self.dy,-self.pZ+self.dz), None)]),
                                        _Polygon([_Vertex(_Vector(-self.pX-self.dx_y-self.dx_z,-self.pY-self.dy,-self.pZ+self.dz), None),
                                                  _Vertex(_Vector(-self.pX-self.dx_y+self.dx_z,-self.pY+self.dy, self.pZ-self.dz), None),
                                                  _Vertex(_Vector(-self.pX+self.dx_y+self.dx_z, self.pY+self.dy, self.pZ-self.dz), None),
                                                  _Vertex(_Vector(-self.pX+self.dx_y-self.dx_z, self.pY-self.dy,-self.pZ+self.dz), None)]),
                                        _Polygon([_Vertex(_Vector( self.pX-self.dx_y-self.dx_z,-self.pY-self.dy,-self.pZ+self.dz), None),
                                                  _Vertex(_Vector( self.pX+self.dx_y-self.dx_z, self.pY-self.dy,-self.pZ+self.dz), None),
                                                  _Vertex(_Vector( self.pX+self.dx_y+self.dx_z, self.pY+self.dy, self.pZ-self.dz), None),
                                                  _Vertex(_Vector( self.pX-self.dx_y+self.dx_z,-self.pY+self.dy, self.pZ-self.dz), None)])])


        return self.mesh


class Trd(SolidBase) :
    """
    Constructs a trapezoid.

    Inputs:
        name:  string, name of the volume
        pDx1:  float, half-length along x at the surface positioned at -dz
        pDx2:  float, half-length along x at the surface postitioned at +dz
        pDy1:  float, half-length along y at the surface positioned at -dz
        pDy2:  float, half-length along y at the surface positioned at +dz
        dz:    float, half-length along the z axis
    """
    def __init__(self, name, pDx1, pDx2, pDy1, pDy2, pDz) :
        self.type   = 'trd'
        self.name   = name
        self.pX1    = pDx1
        self.pX2    = pDx2
        self.pY1    = pDy1
        self.pY2    = pDy2
        self.pZ     = pDz

    def pycsgmesh(self) :
        self.mesh  = _CSG.fromPolygons([_Polygon([_Vertex(_Vector(-self.pX1,-self.pY1,-self.pZ), None),
                                                  _Vertex(_Vector(-self.pX1, self.pY1,-self.pZ), None),
                                                  _Vertex(_Vector( self.pX1, self.pY1,-self.pZ), None),
                                                  _Vertex(_Vector( self.pX1,-self.pY1,-self.pZ), None)]),
                                        _Polygon([_Vertex(_Vector(-self.pX2,-self.pY2, self.pZ), None),
                                                  _Vertex(_Vector( self.pX2,-self.pY2, self.pZ), None),
                                                  _Vertex(_Vector( self.pX2, self.pY2, self.pZ), None),
                                                  _Vertex(_Vector(-self.pX2, self.pY2, self.pZ), None)]),
                                        _Polygon([_Vertex(_Vector(-self.pX1,-self.pY1,-self.pZ), None),
                                                  _Vertex(_Vector( self.pX1,-self.pY1,-self.pZ), None),
                                                  _Vertex(_Vector( self.pX2,-self.pY2, self.pZ), None),
                                                  _Vertex(_Vector(-self.pX2,-self.pY2, self.pZ), None)]),
                                        _Polygon([_Vertex(_Vector(-self.pX1, self.pY1,-self.pZ), None),
                                                  _Vertex(_Vector(-self.pX2, self.pY2, self.pZ), None),
                                                  _Vertex(_Vector( self.pX2, self.pY2, self.pZ), None),
                                                  _Vertex(_Vector( self.pX1, self.pY1,-self.pZ), None)]),
                                        _Polygon([_Vertex(_Vector(-self.pX1,-self.pY1,-self.pZ), None),
                                                  _Vertex(_Vector(-self.pX2,-self.pY2, self.pZ), None),
                                                  _Vertex(_Vector(-self.pX2, self.pY2, self.pZ), None),
                                                  _Vertex(_Vector(-self.pX1, self.pY1,-self.pZ), None)]),
                                        _Polygon([_Vertex(_Vector( self.pX1,-self.pY1,-self.pZ), None),
                                                  _Vertex(_Vector( self.pX1, self.pY1,-self.pZ), None),
                                                  _Vertex(_Vector( self.pX2, self.pY2, self.pZ), None),
                                                  _Vertex(_Vector( self.pX2,-self.pY2, self.pZ), None)])])

        return self.mesh


class ExtrudedSolid(SolidBase):
    def __init__(self, name, pPolygon, pZslices):
        """
        pPoligon: List of lists with the x-y coordinates of vertices for the polygon.
        pZslices: List of lists with z-coordinate of a slice, slice offsets in x-y and scaling

        Example: Triangular prism with 2 slices
        pPoligon = [[x1,y1],[x2,y2],[x3,v3]] - vertices of polygon in clockwise order
        zSlices  = [[z1,[offsx1, offsy1],scale1],[z2,[offsx2, offsy2],scale2]]
        """
        self.type     = 'extrudedsolid'
        self.name     = name
        self.zpos     = [zslice[0] for zslice in pZslices]
        self.x_offs   = [zslice[1][0] for zslice in pZslices]
        self.y_offs   = [zslice[1][1] for zslice in pZslices]
        self.scale    = [zslice[2] for zslice in pZslices]
        self.vertices = pPolygon
        self.nslices  = len(pZslices)

    def pycsgmesh(self):
        polygons  = []
        polygonsT = []
        polygonsB = []

        polygonsT.append(_Polygon([_Vertex(_Vector(self.scale[-1]*vert[0]+self.x_offs[-1],self.scale[-1]*vert[1]+self.y_offs[-1],self.zpos[-1]),None) for vert in list(reversed(self.vertices))]))
        polygonsB.append(_Polygon([_Vertex(_Vector(self.scale[0]*vert[0]+self.x_offs[0], self.scale[0]*vert[1]+self.y_offs[0],self.zpos[0]),None) for vert in self.vertices]))

        polygons.extend(polygonsB)

        for l in range(1, self.nslices):
            maxn = len(self.vertices)

            for n in range(maxn):
                n_up = (n+1)%maxn
                polygons.append(_Polygon([_Vertex(_Vector( self.scale[l]*self.vertices[n][0]+self.x_offs[l], self.scale[l]*self.vertices[n][1]+self.y_offs[l],self.zpos[l]), None),
                                          _Vertex(_Vector( self.scale[l]*self.vertices[n_up][0]+self.x_offs[l],self.scale[l]*self.vertices[n_up][1]+self.y_offs[l],self.zpos[l]), None),
                                          _Vertex(_Vector( self.scale[l-1]*self.vertices[n_up][0]+self.x_offs[l-1],self.scale[l-1]*self.vertices[n_up][1]+self.y_offs[l-1],self.zpos[l-1]), None),
                                          _Vertex(_Vector( self.scale[l-1]*self.vertices[n][0]+self.x_offs[l-1],self.scale[l-1]*self.vertices[n][1]+self.y_offs[l-1],self.zpos[l-1]), None)]))

        polygons.extend(polygonsT)

        self.mesh = _CSG.fromPolygons(polygons)

        return self.mesh


class Polycone(SolidBase) :
    def __init__(self, name, pSPhi, pDPhi, pZpl, pRMin, pRMax, nslice=16) :
        """
        Constructs a solid of rotation using an arbitrary 2D surface.

        Inputs:
          name:   string, name of the volume
          pSPhi:  float, starting rotation angle in radians
          pDPhi:  float, total rotation angle in radius
          pZPlns: list, z-positions of planes used
          pRInr : list, inner radii of surface at each z-plane
          pROut : list, outer radii of surface at each z-plane
        """
        self.type    = 'polycone'
        self.name    = name
        self.pSPhi   = pSPhi
        self.pDPhi   = pDPhi
        self.pZpl    = pZpl
        self.pRMin   = pRMin
        self.pRMax   = pRMax
        self.nslice  = nslice


    def pycsgmesh(self):

        polygons = []

        dPhi  = 2*_np.pi/self.nslice
        stacks  = len(self.pZpl)
        slices  = self.nslice

        def appendVertex(vertices, theta, z, r, norm=[]):
            c = _Vector([0,0,0])
            x = r*_np.cos(theta)
            y = r*_np.sin(theta)

            d = _Vector(
                x,
                y,
                z)

            if not norm:
                n = d
            else:
                n = _Vector(norm)
            vertices.append(_Vertex(c.plus(d), None))

        rinout    = [self.pRMin, self.pRMax]
        meshinout = []

        offs = 1.e-25 #Small offset to avoid point degenracy when the radius is zero. TODO: make more robust
        for R in rinout:
            for j0 in range(stacks-1):
                j1 = j0 + 0.5
                j2 = j0 + 1
                r0 = R[j0] + offs
                r2 = R[j2] + offs
                for i0 in range(slices):
                    i1 = i0 + 0.5
                    i2 = i0 + 1
                    k0 = i0 if R == self.pRMax else i2  #needed to ensure the surface normals on the inner and outer surface are obeyed
                    k1 = i2 if R == self.pRMax else i0
                    vertices = []
                    appendVertex(vertices, k0 * dPhi + self.pSPhi, self.pZpl[j0], r0)
                    appendVertex(vertices, k1 * dPhi + self.pSPhi, self.pZpl[j0], r0)
                    appendVertex(vertices, k1 * dPhi + self.pSPhi, self.pZpl[j2], r2)
                    appendVertex(vertices, k0 * dPhi + self.pSPhi, self.pZpl[j2], r2)

                    polygons.append(_Polygon(vertices))

        for i0 in range(slices):
            i1 = i0 + 0.5
            i2 = i0 + 1
            vertices_t = []
            vertices_b = []

            appendVertex(vertices_t, i2 * dPhi + self.pSPhi, self.pZpl[-1], self.pRMin[-1]+offs)
            appendVertex(vertices_t, i0 * dPhi + self.pSPhi, self.pZpl[-1], self.pRMin[-1]+offs)
            appendVertex(vertices_t, i0 * dPhi + self.pSPhi, self.pZpl[-1], self.pRMax[-1]+offs)
            appendVertex(vertices_t, i2 * dPhi + self.pSPhi, self.pZpl[-1], self.pRMax[-1]+offs)
            polygons.append(_Polygon(vertices_t))

            appendVertex(vertices_b, i0 * dPhi + self.pSPhi, self.pZpl[0], self.pRMin[0]+offs)
            appendVertex(vertices_b, i2 * dPhi + self.pSPhi, self.pZpl[0], self.pRMin[0]+offs)
            appendVertex(vertices_b, i2 * dPhi + self.pSPhi, self.pZpl[0], self.pRMax[0]+offs)
            appendVertex(vertices_b, i0 * dPhi + self.pSPhi, self.pZpl[0], self.pRMax[0]+offs)
            polygons.append(_Polygon(vertices_b))

        self.mesh     = _CSG.fromPolygons(polygons)

        wrmax    = 3*max([abs(r) for r in self.pRMax]) #ensure intersection wedge is much bigger than solid
        wzlength = 3*max([abs(z) for z in self.pZpl])

        if self.pDPhi != 2*_np.pi:
            pWedge = Wedge("wedge_temp",wrmax, self.pSPhi, self.pDPhi+self.pSPhi, wzlength).pycsgmesh()
            self.mesh = pWedge.intersect(self.mesh)

        return self.mesh



class Sphere(SolidBase) :
    def __init__(self, name, pRmin, pRmax, pSPhi, pDPhi, pSTheta, pDTheta, nslice = 16, nstack = 8) :
        """
        Constructs a section of a spherical shell.

        Inputs:
          name:    string, name of the volume
          pRmin:   float, innner radius of the shell
          pRmax:   float, outer radius of the shell
          pSPhi:   float, starting phi angle in radians
          pSTheta: float, starting theta angle in radians
          pDPhi:   float, total phi angle in radians
          pDTheta: float, total theta angle in radians
        """
        self.type    = 'sphere'
        self.name    = name
        self.pRmin   = pRmin
        self.pRmax   = pRmax
        self.pSPhi   = pSPhi
        self.pDPhi   = pDPhi
        self.pSTheta = pSTheta
        self.pDTheta = pDTheta
        self.nslice  = nslice
        self.nstack  = nstack


    def pycsgmesh(self):
        polygons = []

        def appendVertex(vertices, theta, phi, r):
            if r > 0:
                d = _Vector(
                    _np.cos(theta) * _np.sin(phi),
                    _np.cos(phi),
                    _np.sin(theta) * _np.sin(phi))
                vertices.append(_Vertex(c.plus(d.times(r)), None))

            else:
                vertices.append(_Vertex(c, None))


        c      = _Vector([0,0,0])
        slices = self.nslice
        stacks = self.nstack

        dTheta = self.pDTheta / float(slices)
        dPhi = self.pDPhi / float(stacks)

        sthe = self.pSTheta
        sphi = self.pSPhi

        botPoleIn = 1 if sphi%_np.pi == 0 else 0
        topPoleIn = 1 if sphi+self.pDPhi >= _np.pi else 0

        r = self.pRmax
        if botPoleIn:                               #if poles are present mesh them with triangles to avoid point degenracy on pole
            j0 = 0
            j1 = j0 + 1
            for i0 in range(0, slices):
                i1 = i0 + 1
                #  +--+
                #  | /
                #  |/
                #  +
                vertices = []
                appendVertex(vertices, i0 * dTheta + sthe, j0 * dPhi + sphi, r)
                appendVertex(vertices, i1 * dTheta + sthe, j1 * dPhi + sphi, r)
                appendVertex(vertices, i0 * dTheta + sthe, j1 * dPhi + sphi, r)
                polygons.append(_Polygon(vertices))

        if topPoleIn:
            j0 = stacks - 1
            j1 = j0 + 1
            for i0 in range(0, slices):
                i1 = i0 + 1
                #  +
                #  |\
                #  | \
                #  +--+
                vertices = []
                appendVertex(vertices, i0 * dTheta + sthe, j0 * dPhi + sphi, r)
                appendVertex(vertices, i1 * dTheta + sthe, j0 * dPhi + sphi, r)
                appendVertex(vertices, i0 * dTheta + sthe, j1 * dPhi + sphi, r)
                polygons.append(_Polygon(vertices))

        for j0 in range(botPoleIn, stacks - topPoleIn):
            j1 = j0 + 0.5
            j2 = j0 + 1
            for i0 in range(0, slices):
                i1 = i0 + 0.5
                i2 = i0 + 1
                #  +---+
                #  |\ /|
                #  | x |
                #  |/ \|
                #  +---+
                verticesN = []
                appendVertex(verticesN, i1 * dTheta + sthe, j1 * dPhi + sphi, r)
                appendVertex(verticesN, i2 * dTheta + sthe, j2 * dPhi + sphi, r)
                appendVertex(verticesN, i0 * dTheta + sthe, j2 * dPhi + sphi, r)
                polygons.append(_Polygon(verticesN))
                verticesS = []
                appendVertex(verticesS, i1 * dTheta + sthe, j1 * dPhi + sphi, r)
                appendVertex(verticesS, i0 * dTheta + sthe, j0 * dPhi + sphi, r)
                appendVertex(verticesS, i2 * dTheta + sthe, j0 * dPhi + sphi, r)
                polygons.append(_Polygon(verticesS))
                verticesW = []
                appendVertex(verticesW, i1 * dTheta + sthe, j1 * dPhi + sphi, r)
                appendVertex(verticesW, i0 * dTheta + sthe, j2 * dPhi + sphi, r)
                appendVertex(verticesW, i0 * dTheta + sthe, j0 * dPhi + sphi, r)
                polygons.append(_Polygon(verticesW))
                verticesE = []
                appendVertex(verticesE, i1 * dTheta + sthe, j1 * dPhi + sphi, r)
                appendVertex(verticesE, i2 * dTheta + sthe, j0 * dPhi + sphi, r)
                appendVertex(verticesE, i2 * dTheta + sthe, j2 * dPhi + sphi, r)
                polygons.append(_Polygon(verticesE))

        for i0 in range(0, slices):
            i1 = i0 + 1
            vertices = []

            if not topPoleIn:
                appendVertex(vertices, i1 * dTheta + sthe, stacks * dPhi+sphi, r)
                appendVertex(vertices, 0.0, 0.0, 0.0)
                appendVertex(vertices, i0 * dTheta + sthe, stacks * dPhi+sphi, r)
                polygons.append(_Polygon(vertices))

            vertices = []

            if not botPoleIn:
                appendVertex(vertices, i0 * dTheta + sthe, sphi, r)
                appendVertex(vertices, 0.0, 0.0, 0.0)
                appendVertex(vertices, i1 * dTheta + sthe, sphi, r)
                polygons.append(_Polygon(vertices))


        if self.pDTheta%(2*_np.pi) != 0:
            for j0 in range(0, stacks):
                j1 = j0 + 1
                vertices = []

                appendVertex(vertices, sthe, j1 * dPhi + sphi, r)
                appendVertex(vertices, 0.0, 0.0, 0.0)
                appendVertex(vertices, sthe, j0 * dPhi + sphi, r)
                polygons.append(_Polygon(vertices))

                vertices = []

                appendVertex(vertices, slices * dTheta + sthe, j0 * dPhi + sphi, r)
                appendVertex(vertices, 0, 0, 0)
                appendVertex(vertices, slices * dTheta + sthe, j1 * dPhi + sphi, r)
                polygons.append(_Polygon(vertices))


        self.mesh = _CSG.fromPolygons(polygons)

        if self.pRmin:
            mesh_inner   = _CSG.sphere(radius=self.pRmin, slices=self.nslice, stacks=self.nstack)
            self.mesh   = self.mesh.subtract(mesh_inner)

        return self.mesh


    def gdml(selfs):
        pass


class Ellipsoid(SolidBase) :
    def __init__(self, name, pxSemiAxis, pySemiAxis, pzSemiAxis, pzBottomCut, pzTopCut, nslice = 16, nstack = 8) :
        """
        Constructs an ellipsoid optinoally cut by planes perpendicular to the z-axis.

        Inputs:
          name:    string, name of the volume
          pxSemiAxis:  float, length of x semi axis
          pySemiAxis:  float, length of y semi axis
          pzSemiAxis:  float, length of z semi axis
          pzBottomCut: float, z-position of bottom cut plane
          pzTopCut:    float, z-position of top cut plane
        """
        self.type        = 'ellipsoid'
        self.name        = name
        self.pxSemiAxis  = pxSemiAxis
        self.pySemiAxis  = pySemiAxis
        self.pzSemiAxis  = pzSemiAxis
        self.pzBottomCut = pzBottomCut
        self.pzTopCut    = pzTopCut
        self.nslice      = nslice
        self.nstack      = nstack

    def pycsgmesh(self):

        def appendVertex(vertices, u, v):
            d = _Vector(
                self.pxSemiAxis*_np.cos(u)*_np.sin(v),
                self.pySemiAxis*_np.cos(u)*_np.cos(v),
                self.pzSemiAxis*_np.sin(u))

            vertices.append(_Vertex(c.plus(d), None))

        polygons = []

        c      = _Vector([0,0,0])
        slices = self.nslice
        stacks = self.nstack

        du     = _np.pi / float(slices)
        dv     = 2*_np.pi / float(stacks)

        su     = -_np.pi/2
        sv     = -_np.pi

        for j0 in range(0, slices):
            j1 = j0 + 0.5
            j2 = j0 + 1
            for i0 in range(0, slices):
                i1 = i0 + 0.5
                i2 = i0 + 1

                verticesN = []
                appendVertex(verticesN, i1 * du + su, j1 * dv + sv)
                appendVertex(verticesN, i2 * du + su, j2 * dv + sv)
                appendVertex(verticesN, i0 * du + su, j2 * dv + sv)
                polygons.append(_Polygon(verticesN))
                verticesS = []
                appendVertex(verticesS, i1 * du + su, j1 * dv + sv)
                appendVertex(verticesS, i0 * du + su, j0 * dv + sv)
                appendVertex(verticesS, i2 * du + su, j0 * dv + sv)
                polygons.append(_Polygon(verticesS))
                verticesW = []
                appendVertex(verticesW, i1 * du + su, j1 * dv + sv)
                appendVertex(verticesW, i0 * du + su, j2 * dv + sv)
                appendVertex(verticesW, i0 * du + su, j0 * dv + sv)
                polygons.append(_Polygon(verticesW))
                verticesE = []
                appendVertex(verticesE, i1 * du + su, j1 * dv + sv)
                appendVertex(verticesE, i2 * du + su, j0 * dv + sv)
                appendVertex(verticesE, i2 * du + su, j2 * dv + sv)
                polygons.append(_Polygon(verticesE))

        self.mesh  = _CSG.fromPolygons(polygons)

        return self.mesh

    def gdml(selfs):
        pass

class Trap(SolidBase) :
    def __init__(self, name, pDz, pTheta, pDPhi, pDy1, pDx1, pDx2, pAlp1, pDy2, pDx3, pDx4, pAlp2) :
        """
        Constructs a general trapezoid.

        Inputs:
          name:   string, name of the volume
          pDz:    float, half length along z
          pDx1:   float, half length along x of the side at y=-pDy1
          pDx2:   float, half length along x of the side at y=+pDy1
          pTheta: float, polar angle of the line joining the centres of the faces at -/+pDz
          pPhi:   float, azimuthal angle of the line joining the centres of the faces at -/+pDz
          pDy1:   float, half-length at -pDz
          pDy2:   float, half-length at +pDz
          pDx3:   float, halg-length of the side at y=-pDy2 of the face at +pDz
          pDx4:   float, halg-length of the side at y=+pDy2 of the face at +pDz
          pAlp1:  float, angle wrt the y axi from the centre of the side (lower endcap)
          pAlp2:  float, angle wrt the y axi from the centre of the side (upper endcap)
        """
        self.type    = "trap"
        self.name    = name
        self.pDz     = pDz
        self.pTheta  = pTheta
        self.pDPhi   = pDPhi
        self.pDy1    = pDy1
        self.pDx1    = pDx1
        self.pDx2    = pDx2
        self.pAlp1   = pAlp1
        self.pDy2    = pDy2
        self.pDx3    = pDx3
        self.pDx4    = pDx4
        self.pAlp2   = pAlp2

    def pycsgmesh(self):

        def listSub(lista, listb):
            result = [a_i - b_i for a_i, b_i in zip(lista, listb)]
            return result

        def listAdd(lista, listb):
            result = [a_i + b_i for a_i, b_i in zip(lista, listb)]
            return result

        hlZ  = self.pDz

        X1   = self.pDx1 #at -pDz
        X2   = self.pDx2
        Y1   = self.pDy1 #at -pDz

        Y2   = self.pDy2
        X3   = self.pDx3
        X4   = self.pDx4

        dX  = 2*_np.sin(self.pTheta)*self.pDz
        dY  = 2*_np.sin(self.pDPhi)*self.pDz

        poly0 = [[-X2,-Y1,-hlZ],[-X1,Y1,-hlZ],[X1,Y1,-hlZ],[X2,-Y1,-hlZ]]
        poly1 = [[-X3,-Y2,hlZ],[-X4,Y2,hlZ],[X4,Y2,hlZ],[X3,-Y2,hlZ]]

        A0=0.0
        A1=0.0

        #Accumulate signed area of top and bottom face quadrilaterals
        for j in range(len(poly0)-1):
            i0  = j
            i1  = i0 + 1
            A0 += (1./2.)*(poly0[i0][0]*poly0[i1][1]-poly0[i1][0]*poly0[i0][1])
            A1 += (1./2.)*(poly1[i0][0]*poly1[i1][1]-poly1[i1][0]*poly1[i0][1])

        Xc0 = 0.0
        Yc0 = 0.0
        Xc1 = 0.0
        Yc1 = 0.0

        # Obtain centroids of top and bottom quadrilaterals
        for j in range(len(poly0)-1):
            i0 = j
            i1 = i0+1
            Xc0   += (1/(6*A0))*(poly0[i0][0]+poly0[i1][0])*(poly0[i0][0]*poly0[i1][1]-poly0[i1][0]*poly0[i0][1])
            Yc0   += (1/(6*A0))*(poly0[i0][1]+poly0[i1][1])*(poly0[i0][0]*poly0[i1][1]-poly0[i1][0]*poly0[i0][1])
            Xc1   += (1/(6*A1))*(poly1[i0][0]+poly1[i1][0])*(poly1[i0][0]*poly1[i1][1]-poly1[i1][0]*poly1[i0][1])
            Yc1   += (1/(6*A1))*(poly1[i0][1]+poly1[i1][1])*(poly1[i0][0]*poly1[i1][1]-poly1[i1][0]*poly1[i0][1])

        C0  = [Xc0, Yc0, 0]
        C1  = [Xc1, Yc1, 0]

        #Center in X-Y plane
        poly0  = [listSub(vert, C0) for vert in poly0]
        poly1  = [listSub(vert, C1) for vert in poly1]


        #Slant faces
        for i in range(len(poly0)):
            vert = poly0[i]
            y    = vert[1]
            z    = vert[2]
            x = vert[0] + y*_np.tan(self.pAlp1)

            poly0[i] = [x,y,z]

        for i in range(len(poly1)):
            vert = poly1[i]
            y    = vert[1]
            z    = vert[2]
            x = vert[0] + y*_np.tan(self.pAlp2)

            poly1[i] = [x,y,z]

        #Translate to orginal coordinates
        poly0  = [listAdd(vert, C0) for vert in poly0]
        poly1  = [listAdd(vert, C1) for vert in poly1]

        dXY    = [dX/2, dY/2, 0]
        poly1  = [listAdd(vert, dXY) for vert in poly1]
        poly0  = [listSub(vert, dXY) for vert in poly0]

        polygons = []

        #Top face
        polygons.extend([_Polygon([_Vertex(_Vector(poly1[3][0], poly1[3][1], poly1[3][2]), None),
                                   _Vertex(_Vector(poly1[2][0], poly1[2][1], poly1[2][2]), None),
                                   _Vertex(_Vector(poly1[1][0], poly1[1][1], poly1[1][2]), None),
                                   _Vertex(_Vector(poly1[0][0], poly1[0][1], poly1[0][2]), None)])])

        #Bottom face
        polygons.extend([_Polygon([_Vertex(_Vector(poly0[0][0], poly0[0][1], poly0[0][2]), None),
                                   _Vertex(_Vector(poly0[1][0], poly0[1][1], poly0[1][2]), None),
                                   _Vertex(_Vector(poly0[2][0], poly0[2][1], poly0[2][2]), None),
                                   _Vertex(_Vector(poly0[3][0], poly0[3][1], poly0[3][2]), None)])])

        #Side faces
        polygons.extend([_Polygon([_Vertex(_Vector(poly1[1][0], poly1[1][1], poly1[1][2]), None),
                                   _Vertex(_Vector(poly0[1][0], poly0[1][1], poly0[1][2]), None),
                                   _Vertex(_Vector(poly0[0][0], poly0[0][1], poly0[0][2]), None),
                                   _Vertex(_Vector(poly1[0][0], poly1[0][1], poly1[0][2]), None)])])
        polygons.extend([_Polygon([_Vertex(_Vector(poly1[2][0], poly1[2][1], poly1[2][2]), None),
                                   _Vertex(_Vector(poly0[2][0], poly0[2][1], poly0[2][2]), None),
                                   _Vertex(_Vector(poly0[1][0], poly0[1][1], poly0[1][2]), None),
                                   _Vertex(_Vector(poly1[1][0], poly1[1][1], poly1[1][2]), None)])])
        polygons.extend([_Polygon([_Vertex(_Vector(poly1[3][0], poly1[3][1], poly1[3][2]), None),
                                   _Vertex(_Vector(poly0[3][0], poly0[3][1], poly0[3][2]), None),
                                   _Vertex(_Vector(poly0[2][0], poly0[2][1], poly0[2][2]), None),
                                   _Vertex(_Vector(poly1[2][0], poly1[2][1], poly1[2][2]), None)])])
        polygons.extend([_Polygon([_Vertex(_Vector(poly1[0][0], poly1[0][1], poly1[0][2]), None),
                                   _Vertex(_Vector(poly0[0][0], poly0[0][1], poly0[0][2]), None),
                                   _Vertex(_Vector(poly0[3][0], poly0[3][1], poly0[3][2]), None),
                                   _Vertex(_Vector(poly1[3][0], poly1[3][1], poly1[3][2]), None)])])


        self.mesh  = _CSG.fromPolygons(polygons)

        return self.mesh

class Torus(SolidBase) :
    def __init__(self, name, pRmin, pRmax, pRtor, pSPhi, pDPhi, nslice=16, nstack=16) :
        """
        Constructs a torus.

        Inputs:
          name:   string, name of the volume
          pRmin:  float, innder radius
          pRmax:  float, outer radius
          pRtor:  float, swept radius of torus
          pSphi:  float, start phi angle
          pDPhi:  float, delta angle
        """
        self.type    = 'torus'
        self.name    = name
        self.pRmin   = pRmin
        self.pRmax   = pRmax
        self.pRtor   = pRtor
        self.pSPhi   = pSPhi
        self.pDPhi   = pDPhi
        self.nslice = nslice
        self.nstack = nstack


    def pycsgmesh(self):

        polygons = []

        dTheta  = 2*_np.pi/self.nstack
        dPhi    = 2*_np.pi/self.nslice
        sphi    = self.pSPhi
        stacks  = self.nstack
        slices  = self.nslice

        def appendVertex(vertices, theta, phi, r):
            c = _Vector([0,0,0])
            x = r*_np.cos(theta)+self.pRtor
            z = r*_np.sin(theta)
            y = 0
            x_rot = _np.cos(phi)*x - _np.sin(phi)*y
            y_rot = _np.sin(phi)*x + _np.cos(phi)*y

            d = _Vector(
                x_rot,
                y_rot,
                z)

            vertices.append(_Vertex(c.plus(d), None))

        rinout    = [self.pRmin, self.pRmax]
        meshinout = []

        for r in rinout:
            for j0 in range(slices):
                j1 = j0 + 0.5
                j2 = j0 + 1
                for i0 in range(stacks):
                    i1 = i0 + 0.5
                    i2 = i0 + 1
                    verticesN = []
                    appendVertex(verticesN, i1 * dTheta, j1 * dPhi + sphi, r)
                    appendVertex(verticesN, i2 * dTheta, j2 * dPhi + sphi, r)
                    appendVertex(verticesN, i0 * dTheta, j2 * dPhi + sphi, r)
                    polygons.append(_Polygon(verticesN))
                    verticesS = []
                    appendVertex(verticesS, i1 * dTheta, j1 * dPhi + sphi, r)
                    appendVertex(verticesS, i0 * dTheta, j0 * dPhi + sphi, r)
                    appendVertex(verticesS, i2 * dTheta, j0 * dPhi + sphi, r)
                    polygons.append(_Polygon(verticesS))
                    verticesW = []
                    appendVertex(verticesW, i1 * dTheta, j1 * dPhi + sphi, r)
                    appendVertex(verticesW, i0 * dTheta, j2 * dPhi + sphi, r)
                    appendVertex(verticesW, i0 * dTheta, j0 * dPhi + sphi, r)
                    polygons.append(_Polygon(verticesW))
                    verticesE = []
                    appendVertex(verticesE, i1 * dTheta, j1 * dPhi + sphi, r)
                    appendVertex(verticesE, i2 * dTheta, j0 * dPhi + sphi, r)
                    appendVertex(verticesE, i2 * dTheta, j2 * dPhi + sphi, r)
                    polygons.append(_Polygon(verticesE))

            mesh      = _CSG.fromPolygons(polygons)
            meshinout.append(mesh)
            polygons = []

        if self.pRmin != 0:
            self.mesh  = meshinout[0].subtract(meshinout[1])

        else:
           self.mesh = meshinout[1].inverse()

        if self.pDPhi != 2*_np.pi:
            wrmax    = 3*self.pRtor #make sure intersection wedge is much larger than solid
            wzlength = 5*self.pRmax

            pWedge = Wedge("wedge_temp",wrmax, self.pSPhi, self.pDPhi, wzlength).pycsgmesh()
            self.mesh = pWedge.intersect(self.mesh)


        #topNorm     = _Vector(0,0,1)                              # These are tests of booleans operations, keep here for now
        #botNorm     = _Vector(0,0,-1)
        #pTopCut     = Plane("pTopCut", topNorm, 55).pycsgmesh()
        #pBottomCut  = Plane("pBottomCut" , botNorm, 40).pycsgmesh()
        #self.mesh   = self.mesh.subtract(pBottomCut).subtract(pTopCut)

        return self.mesh

class Polyhedra(SolidBase) :
    def __init__(self, name, phiStart, phiTotal, numSide, numZPlanes, zPlane, rInner, rOuter) :
        """
        Constructs a polyhedra.

        Inputs:
          name:       string, name of the volume
          phiStart:   float, start phi angle
          phiTotal:   float, delta phi angle
          numSide:    int, number of sides
          numZPlanes: int, number of planes along z
          zPlane:     list, position of z planes
          rInner:     list, tangent distance to inner surface per z plane
          rOuter:     list, tangent distance to outer surface per z plane

        """
        self.type       = 'polyhedra'
        self.name       = name
        self.phiStart   = phiStart
        self.phiTotal   = phiTotal
        self.numSide    = numSide
        self.numZPlanes = numZPlanes
        self.zPlane     = zPlane
        self.rInner     = rInner
        self.rOuter     = rOuter


    def pycsgmesh(self):

        fillfrac  = self.phiTotal/(2*_np.pi)
        slices    = (self.numSide)/fillfrac

        ph        = Polycone(self.name, self.phiStart, self.phiTotal, self.zPlane, self.rInner, self.rOuter, nslice=int(_np.ceil(slices)))
        self.mesh = ph.pycsgmesh()

        return self.mesh

class EllipticalTube(SolidBase) :
    def __init__(self, name, pDx, pDy, pDz, nslice=16, nstack=16) :
        """
        Constructs a tube of elliptical cross-section.

        Inputs:
          name: string, name of the volume
          pDx:  float, half-length in x
          pDy:  float, half-length in y
          pDz:  float, half-length in z
        """
        self.type   = 'ellipticaltube'
        self.name   = name
        self.pDx    = pDx
        self.pDy    = pDy
        self.pDz    = pDz
        self.nslice = nslice
        self.nstack = nstack

    def pycsgmesh(self):
        polygons = []

        sz      = -self.pDz
        dz      = 2*self.pDz/self.nstack
        dTheta  = 2*_np.pi/self.nslice
        stacks  = self.nstack
        slices  = self.nslice

        def appendVertex(vertices, theta, z, dx=self.pDx, dy=self.pDy, norm=[]):
            c = _Vector([0,0,0])
            x = dx*_np.cos(theta)
            y = dy*_np.sin(theta)

            d = _Vector(
                x,
                y,
                z)

            if not norm:
                n = d
            else:
                n = _Vector(norm)
            vertices.append(_Vertex(c.plus(d), d))


        for j0 in range(slices):
            j1 = j0 + 0.5
            j2 = j0 + 1
            for i0 in range(stacks):
                i1 = i0 + 0.5
                i2 = i0 + 1
                verticesN = []
                appendVertex(verticesN, i1 * dTheta, j1 * dz + sz)
                appendVertex(verticesN, i2 * dTheta, j2 * dz + sz)
                appendVertex(verticesN, i0 * dTheta, j2 * dz + sz)
                polygons.append(_Polygon(verticesN))
                verticesS = []
                appendVertex(verticesS, i1 * dTheta, j1 * dz + sz)
                appendVertex(verticesS, i0 * dTheta, j0 * dz + sz)
                appendVertex(verticesS, i2 * dTheta, j0 * dz + sz)
                polygons.append(_Polygon(verticesS))
                verticesW = []
                appendVertex(verticesW, i1 * dTheta, j1 * dz + sz)
                appendVertex(verticesW, i0 * dTheta, j2 * dz + sz)
                appendVertex(verticesW, i0 * dTheta, j0 * dz + sz)
                polygons.append(_Polygon(verticesW))
                verticesE = []
                appendVertex(verticesE, i1 * dTheta, j1 * dz + sz)
                appendVertex(verticesE, i2 * dTheta, j0 * dz + sz)
                appendVertex(verticesE, i2 * dTheta, j2 * dz + sz)
                polygons.append(_Polygon(verticesE))

        for i0 in range(0, slices):
            i1 = i0 + 1

            vertices = []

            appendVertex(vertices, i0 * dTheta, sz, norm=[0,0,1])
            appendVertex(vertices, 0, sz, dx = 0, dy = 0, norm=[0,0,1])
            appendVertex(vertices, i1 * dTheta, sz, norm=[0,0,1])
            polygons.append(_Polygon(vertices))

            vertices = []
            appendVertex(vertices, i1 * dTheta, stacks * dz + sz, norm=[0,0,-1])
            appendVertex(vertices, 0, slices*dz + sz, dx = 0, dy = 0, norm=[0,0,-1])
            appendVertex(vertices, i0 * dTheta, stacks * dz + sz, norm=[0,0,-1])
            polygons.append(_Polygon(vertices))

        self.mesh  = _CSG.fromPolygons(polygons)

        return self.mesh

class EllipticalCone(SolidBase) :
    def __init__(self, name, pxSemiAxis, pySemiAxis, zMax, pzTopCut, nslice=16, nstack=16) :
        """
        Constructs a cone with elliptical cross-section.

        Inputs:
          name:       string, name of the volume
          pxSemiAxis: float, semiaxis in x
          pySemiAxis: float, semiaxis in y
          zMax:       float, height of cone
          pzTopCut:   float, z-position of upper
        """

        self.type       = 'ellipticalcone'
        self.name       = name
        self.pxSemiAxis = pxSemiAxis
        self.pySemiAxis = pySemiAxis
        self.zMax       = zMax
        self.pzTopCut   = pzTopCut
        self.nslice     = nslice
        self.nstack     = nslice


    def pycsgmesh(self):
        polygons = []

        sz      = -self.zMax/2.
        dz      = self.zMax/self.nstack
        dTheta  = 2*_np.pi/self.nslice
        stacks  = self.nstack
        slices  = self.nslice

        def appendVertex(vertices, theta, z, dx=self.pxSemiAxis,dy=self.pySemiAxis, norm=[]):
            c = _Vector([0,0,0])
            x = dx*(((self.zMax - z)/self.zMax)*_np.cos(theta)) #generate points on an ellipse
            y = dy*(((self.zMax - z)/self.zMax)*_np.sin(theta))
            d = _Vector(
                x,
                y,
                z)
            if not norm:
                n = d
            else:
                n = _Vector(norm)
            vertices.append(_Vertex(c.plus(d), d))


        for j0 in range(slices):
            j1 = j0 + 0.5
            j2 = j0 + 1
            for i0 in range(stacks):
                i1 = i0 + 0.5
                i2 = i0 + 1
                verticesN = []
                appendVertex(verticesN, i1 * dTheta, j1 * dz + sz)
                appendVertex(verticesN, i2 * dTheta, j2 * dz + sz)
                appendVertex(verticesN, i0 * dTheta, j2 * dz + sz)
                polygons.append(_Polygon(verticesN))
                verticesS = []
                appendVertex(verticesS, i1 * dTheta, j1 * dz + sz)
                appendVertex(verticesS, i0 * dTheta, j0 * dz + sz)
                appendVertex(verticesS, i2 * dTheta, j0 * dz + sz)
                polygons.append(_Polygon(verticesS))
                verticesW = []
                appendVertex(verticesW, i1 * dTheta, j1 * dz + sz)
                appendVertex(verticesW, i0 * dTheta, j2 * dz + sz)
                appendVertex(verticesW, i0 * dTheta, j0 * dz + sz)
                polygons.append(_Polygon(verticesW))
                verticesE = []
                appendVertex(verticesE, i1 * dTheta, j1 * dz + sz)
                appendVertex(verticesE, i2 * dTheta, j0 * dz + sz)
                appendVertex(verticesE, i2 * dTheta, j2 * dz + sz)
                polygons.append(_Polygon(verticesE))

        for i0 in range(0, slices):
            i1 = i0 + 1

            vertices = []

            appendVertex(vertices, i0 * dTheta, sz, norm=[0,0,1])
            appendVertex(vertices, 0, sz, dx = 0, dy = 0, norm=[0,0,1])
            appendVertex(vertices, i1 * dTheta, sz, norm=[0,0,1])
            polygons.append(_Polygon(vertices))

            vertices = []
            appendVertex(vertices, i1 * dTheta, stacks * dz + sz, norm=[0,0,-1])
            appendVertex(vertices, 0, slices*dz + sz, dx = 0, dy = 0, norm=[0,0,-1])
            appendVertex(vertices, i0 * dTheta, stacks * dz + sz, norm=[0,0,-1])
            polygons.append(_Polygon(vertices))

        self.mesh  = _CSG.fromPolygons(polygons)

        return self.mesh

class Paraboloid(SolidBase) :
    def __init__(self, name, pDz, pR1, pR2, nstack=8, nslice=16) :
        """
        Constructs a paraboloid with possible cuts along the z axis.

        Inputs:
          name: string, name of the volume
          pDz:  float, half-length along z
          pR1:  float, radius at -Dz
          pR2:  float, radius at +Dz (R2 > R1)
        """
        self.type   = 'paraboloid'
        self.name   = name
        self.pDz    = pDz
        self.pR1    = pR1
        self.pR2    = pR2
        self.nstack = nstack
        self.nslice = nslice

    def pycsgmesh(self):
        polygons = []

        sz      = -self.pDz
        dz      = 2*self.pDz/self.nstack
        dTheta  = 2*_np.pi/self.nslice
        stacks  = self.nstack
        slices  = self.nslice

        K1 = (self.pR2**2-self.pR1**2)/(2*self.pDz)
        K2 = (self.pR2**2+self.pR1**2)/2

        def appendVertex(vertices, theta, z, k1=K1, k2=K2, norm=[]):
            if k1 and k2:
                rho = _np.sqrt(k1*z+k2)
            else:
                rho = 0

            c = _Vector([0,0,0])
            x = rho*_np.cos(theta)
            y = rho*_np.sin(theta)

            d = _Vector(
                x,
                y,
                z)

            if not norm:
                n = d
            else:
                n = _Vector(norm)
            vertices.append(_Vertex(c.plus(d), d))


        for j0 in range(stacks):
            j1 = j0 + 0.5
            j2 = j0 + 1
            for i0 in range(slices):
                i1 = i0 + 0.5
                i2 = i0 + 1
                verticesN = []
                appendVertex(verticesN, i1 * dTheta, j1 * dz + sz)
                appendVertex(verticesN, i2 * dTheta, j2 * dz + sz)
                appendVertex(verticesN, i0 * dTheta, j2 * dz + sz)
                polygons.append(_Polygon(verticesN))
                verticesS = []
                appendVertex(verticesS, i1 * dTheta, j1 * dz + sz)
                appendVertex(verticesS, i0 * dTheta, j0 * dz + sz)
                appendVertex(verticesS, i2 * dTheta, j0 * dz + sz)
                polygons.append(_Polygon(verticesS))
                verticesW = []
                appendVertex(verticesW, i1 * dTheta, j1 * dz + sz)
                appendVertex(verticesW, i0 * dTheta, j2 * dz + sz)
                appendVertex(verticesW, i0 * dTheta, j0 * dz + sz)
                polygons.append(_Polygon(verticesW))
                verticesE = []
                appendVertex(verticesE, i1 * dTheta, j1 * dz + sz)
                appendVertex(verticesE, i2 * dTheta, j0 * dz + sz)
                appendVertex(verticesE, i2 * dTheta, j2 * dz + sz)
                polygons.append(_Polygon(verticesE))

        for i0 in range(0, slices):
            i1 = i0 + 1

            vertices = []

            appendVertex(vertices, i0 * dTheta, sz)
            appendVertex(vertices, 0, sz, k1 = 0) #Setting K1=0 forces a zero vector which is used as the center
            appendVertex(vertices, i1 * dTheta, sz)
            polygons.append(_Polygon(vertices))

            vertices = []
            appendVertex(vertices, i1 * dTheta, stacks * dz + sz)
            appendVertex(vertices, 0, stacks*dz + sz, k1 = 0)
            appendVertex(vertices, i0 * dTheta, stacks * dz + sz)
            polygons.append(_Polygon(vertices))

        self.mesh  = _CSG.fromPolygons(polygons)

        return self.mesh

class Hype(SolidBase) :
    def __init__(self, name, innerRadius, outerRadius, innerStereo, outerStereo, halfLenZ, nslice=16, nstack=16) :
        """
        Constructs a tube with hyperbolic profile.

        Inputs:
          name:        string, name of the volume
          innerRadius: float, inner radius
          outerRadius: float, outer radius
          innerStereo: float, inner stereo angle
          outerStereo: float, outer stereo angle
          halfLenZ:    float, half length along z
        """
        self.type        = 'hype'
        self.name        = name
        self.innerRadius = innerRadius
        self.outerRadius = outerRadius
        self.innerStereo = innerStereo
        self.outerStereo = outerStereo
        self.halfLenZ    = halfLenZ
        self.nslice      = nslice
        self.nstack      = nstack


    def pycsgmesh(self):
        dz     = 2*(self.halfLenZ+1.e-6)/self.nstack #make a little bigger as safety for subtractions
        sz     = -self.halfLenZ-1.e-6
        dTheta = 2*_np.pi/self.nslice
        stacks = self.nstack
        slices = self.nslice
        rinout  = [self.innerRadius, self.outerRadius] if self.innerRadius else [self.outerRadius]
        stinout = [self.innerStereo, self.outerStereo]

        polygons = []

        def appendVertex(vertices, theta, z, r, stereo, norm=1):
            c     = _Vector([0,0,0])
            x     = _np.sqrt(r**2+(_np.tan(stereo)*z)**2)
            y     = 0
            x_rot = _np.cos(theta)*x - _np.sin(theta)*y
            y_rot = _np.sin(theta)*x + _np.cos(theta)*y

            d = _Vector(
                x_rot,
                y_rot,
                z)

            if norm == 0:
                n = None
            if norm == 1:
                n = d
            else:
                n = d.negated()

            vertices.append(_Vertex(c.plus(d), n))

        meshinout = []
        for i in range(len(rinout)):
            for j0 in range(stacks):
                j1 = j0 + 0.5
                j2 = j0 + 1
                for i0 in range(slices):
                    nrm = 0

                    i1 = i0 + 0.5
                    i2 = i0 + 1
                    verticesN = []
                    appendVertex(verticesN, i1 * dTheta, j1 * dz + sz, rinout[i], stinout[i], norm=nrm)
                    appendVertex(verticesN, i2 * dTheta, j2 * dz + sz, rinout[i], stinout[i], norm=nrm)
                    appendVertex(verticesN, i0 * dTheta, j2 * dz + sz, rinout[i], stinout[i], norm=nrm)
                    polygons.append(_Polygon(verticesN))
                    verticesS = []
                    appendVertex(verticesS, i1 * dTheta, j1 * dz + sz, rinout[i], stinout[i], norm=nrm)
                    appendVertex(verticesS, i0 * dTheta, j0 * dz + sz, rinout[i], stinout[i], norm=nrm)
                    appendVertex(verticesS, i2 * dTheta, j0 * dz + sz, rinout[i], stinout[i], norm=nrm)
                    polygons.append(_Polygon(verticesS))
                    verticesW = []
                    appendVertex(verticesW, i1 * dTheta, j1 * dz + sz, rinout[i], stinout[i], norm=nrm)
                    appendVertex(verticesW, i0 * dTheta, j2 * dz + sz, rinout[i], stinout[i], norm=nrm)
                    appendVertex(verticesW, i0 * dTheta, j0 * dz + sz, rinout[i], stinout[i], norm=nrm)
                    polygons.append(_Polygon(verticesW))
                    verticesE = []
                    appendVertex(verticesE, i1 * dTheta, j1 * dz + sz, rinout[i], stinout[i], norm=nrm)
                    appendVertex(verticesE, i2 * dTheta, j0 * dz + sz, rinout[i], stinout[i], norm=nrm)
                    appendVertex(verticesE, i2 * dTheta, j2 * dz + sz, rinout[i], stinout[i], norm=nrm)
                    polygons.append(_Polygon(verticesE))

            mesh      = _CSG.fromPolygons(polygons)
            meshinout.append(mesh)
            polygons = []

        for i in range(len(meshinout)):
            wzlength     = 3*self.halfLenZ
            topNorm      = _Vector(0,0,1)
            botNorm      = _Vector(0,0,-1)
            pTopCut      = Plane("pTopCut", topNorm, self.halfLenZ, wzlength).pycsgmesh()
            pBottomCut   = Plane("pBottomCut" , botNorm, -self.halfLenZ, wzlength).pycsgmesh()
            meshinout[i] = meshinout[i].subtract(pTopCut).subtract(pBottomCut)

        if self.innerRadius:
            self.mesh = meshinout[1].subtract(meshinout[0])
        else:
            self.mesh = meshinout[0]

        return self.mesh

class Tet(SolidBase) :
    def __init__(self, name, anchor, p2, p3, p4, degeneracyFlag = False) :
        """
        Constructs a tetrahedra.

        Inputs:
          name:           string, name of the volume
          anchor:         list, anchor point
          p2:             list, point 2
          p3:             list, point 3
          p4:             list, point 4
          degeneracyFlag: bool, indicates degeneracy of points
        """
        self.type    = 'tet'
        self.name    = name
        self.anchor  = anchor
        self.p2      = p2
        self.p3      = p3
        self.p4      = p4
        self.degen   = degeneracyFlag


    def pycsgmesh(self):
        vert_ancr = _Vertex(_Vector(self.anchor[0], self.anchor[1], self.anchor[2]), None)
        base      = [self.p2, self.p3, self.p4]
        vert_base = []

        for i in range(len(base)):
            vert_base.append(_Vertex(_Vector(base[i][0],base[i][1],base[i][2]), None))

        self.mesh = _CSG.fromPolygons([_Polygon([vert_base[2], vert_base[1], vert_base[0]], None),
                                       _Polygon([vert_base[1], vert_ancr, vert_base[0]], None),
                                       _Polygon([vert_base[2], vert_ancr, vert_base[1]], None),
                                       _Polygon([vert_base[0], vert_ancr, vert_base[2]], None)])

        return self.mesh


class TwistedBox(SolidBase) :
    def __init__(self, name, twistedangle, pDx, pDy, pDz, refine=0) :
        """
        Constructs a tube of elliptical cross-section.

        Inputs:
          name:         string, name of the volume
          twistedangle: float, twist angle
          pDx:          float, half-length in x
          pDy:          float, half-length in y
          pDz:          float, half-length in z
          refine:       int, number of steps to iteratively smoothen the mesh
                             by doubling the number of vertices at every step
        """
        self.type = 'twistedbox'
        self.name = name
        self.twistedAngle = twistedangle
        self.pDx    = pDx
        self.pDy    = pDy
        self.pDz    = pDz
        self.refine = refine

    def pycsgmesh(self):
        vert_crd_up = [[-self.pDx, -self.pDy, self.pDz],[self.pDx, -self.pDy, self.pDz],[self.pDx, self.pDy, self.pDz],[-self.pDx, self.pDy, self.pDz]]
        vert_crd_dn = [[-self.pDx, -self.pDy, -self.pDz],[-self.pDx, self.pDy, -self.pDz],[self.pDx, self.pDy, -self.pDz],[self.pDx, -self.pDy, -self.pDz]]

        vert_crd_rot = []

        for i in range(len(vert_crd_up)):
            x_rot = vert_crd_up[i][0]*_np.cos(self.twistedAngle) - vert_crd_up[i][1]*_np.sin(self.twistedAngle)
            y_rot = vert_crd_up[i][0]*_np.sin(self.twistedAngle) + vert_crd_up[i][1]*_np.cos(self.twistedAngle)
            #x_rot = vert_crd_up[i][0]
            #y_rot = vert_crd_up[i][1]
            vert_crd_rot.append([x_rot, y_rot, vert_crd_up[i][2]])

        vert_up = []
        vert_dn = []

        for i in range(len(vert_crd_dn)):
            vert_up.append(_Vertex(_Vector(vert_crd_rot[i][0],vert_crd_rot[i][1],vert_crd_rot[i][2]), None))
            vert_dn.append(_Vertex(_Vector(vert_crd_dn[i][0],vert_crd_dn[i][1],vert_crd_dn[i][2]), None))
        """
        self.mesh = _CSG.fromPolygons([_Polygon([vert_dn[0], vert_dn[1], vert_dn[2], vert_dn[3]], None),
                                       _Polygon([vert_up[0], vert_up[1], vert_up[2], vert_up[3]], None),
                                       _Polygon([vert_dn[0], vert_dn[3], vert_up[1], vert_up[0]], None),
                                       _Polygon([vert_dn[1], vert_up[3], vert_up[2], vert_dn[2]], None),
                                       _Polygon([vert_dn[0], vert_up[0], vert_up[3], vert_dn[1]], None),
                                       _Polygon([vert_dn[3], vert_dn[2], vert_up[2], vert_up[1]], None)])
        #_Polygon([vert_dn[3], vert_dn[2], vert_dn[1], vert_dn[0], vert_dn[3]], None),
        #_Polygon([vert_up[0], vert_up[1], vert_up[2], vert_up[3], vert_up[0]], None),
        for i in range(self.refine):
            self.mesh = self.mesh.refine()
        """

        self.mesh = _CSG.fromPolygons([_Polygon([vert_dn[0], vert_dn[1], vert_dn[2]], None),
                                       _Polygon([vert_dn[2], vert_dn[3], vert_dn[0]], None),

                                       _Polygon([vert_up[0], vert_up[1], vert_up[2]], None),
                                       _Polygon([vert_up[2], vert_up[3], vert_up[0]], None),

                                       _Polygon([vert_dn[0], vert_dn[3], vert_up[1]], None),
                                       _Polygon([vert_dn[0], vert_up[1], vert_up[0]], None),

                                       _Polygon([vert_dn[1], vert_up[2], vert_dn[2]], None),
                                       _Polygon([vert_dn[1], vert_up[3], vert_up[2]], None),

                                       _Polygon([vert_dn[0], vert_up[0], vert_up[3]], None),
                                       _Polygon([vert_up[3], vert_dn[1], vert_dn[0]], None),

                                       _Polygon([vert_dn[3], vert_dn[2], vert_up[2]], None),
                                       _Polygon([vert_up[2], vert_up[1], vert_dn[3]], None)])

        for i in range(self.refine):
            self.mesh = self.mesh.refine()

        return self.mesh



class TwistedTrap(SolidBase) :
    def __init__(self, name, twistedangle, pDz, pTheta, pDPhi, pDy1, pDx1, pDx2, pDy2, pDx3, pDx4, pAlp) :
        """
        Constructs a general trapezoid with a twist around one axis.

        Inputs:
          name:          string, name of the volume
          twisted angle: float, angle of twist (<90 deg)
          pDz:           float, half length along z
          pDx1:          float, half length along x of the side at y=-pDy1
          pDx2:          float, half length along x of the side at y=+pDy1
          pTheta:        float, polar angle of the line joining the centres of the faces at -/+pDz
          pPhi:          float, azimuthal angle of the line joining the centres of the faces at -/+pDz
          pDy1:          float, half-length at -pDz
          pDy2:          float, half-length at +pDz
          pDx3:          float, halg-length of the side at y=-pDy2 of the face at +pDz
          pDx4:          float, halg-length of the side at y=+pDy2 of the face at +pDz
          pAlp:          float, angle wrt the y axi from the centre of the side
        """
        self.type         = 'trap'
        self.name         = name
        self.twistedangle = twistedangle
        self.pDz          = pDz
        self.pTheta       = pTheta
        self.pDPhi        = pDPhi
        self.pDy1         = pDy1
        self.pDx1         = pDx1
        self.pDx2         = pDx2
        self.pAlp1        = pAlp1
        self.pDy2         = pDy2
        self.pDx3         = pDx3
        self.pDx4         = pDx4
        self.pAlp        = pAlp

    def pycsgmesh(self):
        pass


class Union(SolidBase, Boolean) :
    """
    name = name
    obj1 = unrotated, untranslated solid
    obj2 = solid rotated and translated according to tra2
    tra2 = [rot,tra]
    """
    def __init__(self, name, obj1, obj2, tra2):
        self.type = "boolu"
        self.name = name
        self.obj1 = obj1
        self.obj2 = obj2
        self.tra2 = tra2


    def pycsgmesh(self):
        rot = tbxyz(self.tra2[0])
        tlate = self.tra2[1]

        m1 = self.obj1.pycsgmesh()
        m2 = self.obj2.pycsgmesh()
        # In this implementation, passive rotations are positive. This
        # matches Geant4 in which passive rotations are positive for /volumes/.
        # However, Geant4 uses active rotations in boolean operations, this
        # means a factor of -1 must be introduced to rotate in the opposite
        # direction to get the pygdml mesh to match Geant4 correctly.
        m2.rotate(rot[0], -1 * rad2deg(rot[1]))
        m2.translate(tlate)
        self.obj2mesh = m2

        self.mesh = m1.union(m2)
        return self.mesh


class Intersection(SolidBase, Boolean) :
    """
    name = name
    obj1 = unrotated, untranslated solid
    obj2 = solid rotated and translated according to tra2
    tra2 = [rot,tra]
    """
    def __init__(self, name, obj1, obj2, tra2):
        self.type = "booli"
        self.name = name
        self.obj1 = obj1
        self.obj2 = obj2
        self.tra2 = tra2


    def pycsgmesh(self):
        rot   = tbxyz(self.tra2[0])
        tlate = self.tra2[1]

        m1 = self.obj1.pycsgmesh()
        m2 = self.obj2.pycsgmesh()
        # In this implementation, passive rotations are positive. This
        # matches Geant4 in which passive rotations are positive for /volumes/.
        # However, Geant4 uses active rotations in boolean operations, this
        # means a factor of -1 must be introduced to rotate in the opposite
        # direction to get the pygdml mesh to match Geant4 correctly.
        m2.rotate(rot[0], -1 * rad2deg(rot[1]))
        m2.translate(tlate)
        self.obj2mesh = m2

        self.mesh = m1.intersect(m2)
        if not self.mesh.toPolygons():
            raise NullMeshError(self)
        return self.mesh

class Subtraction(SolidBase, Boolean) :
    """
    output = obj1 - obj2
    name = name
    obj1 = unrotated, untranslated solid
    obj2 = solid rotated and translated according to tra2
    tra2 = [rot,tra]
    """
    def __init__(self,name, obj1, obj2, tra2):
        self.type = "bools"
        self.name = name
        self.obj1 = obj1
        self.obj2 = obj2
        self.tra2 = tra2

    def pycsgmesh(self):
        rot = tbxyz(self.tra2[0])
        tlate = self.tra2[1]

        m1 = self.obj1.pycsgmesh()
        m2 = self.obj2.pycsgmesh()
        # In this implementation, passive rotations are positive. This
        # matches Geant4 in which passive rotations are positive for /volumes/.
        # However, Geant4 uses active rotations in boolean operations, this
        # means a factor of -1 must be introduced to rotate in the opposite
        # direction to get the pygdml mesh to match Geant4 correctly.
        m2.rotate(rot[0], -1 * rad2deg(rot[1]))
        m2.translate(tlate)
        self.obj2mesh = m2

        self.mesh = m1.subtract(m2)
        if not self.mesh.toPolygons():
            raise NullMeshError(self)
        return self.mesh


class PolyconePts(SolidBase) :
    def __init__(self, name, pSPhi, pDPhi, pNSide, pNRZ, pR, pZ, nslice=16) :
        """
        Constructs a solid of rotation using an arbitrary 2D surface defined by a series of (r,z) coordinates.

        Inputs:
        name = name
        pSPhi = Angle Phi at start of rotation
        pDPhi = Angle Phi at end of rotation
        pNSide = Number of sides to polygon
        pNRZ = number of (r,z) coordinate points given
        pR = r coordinate list
		pZ = z coordinate list
        """
        self.type    = 'polycone'
        self.name    = name
        self.pSPhi   = pSPhi
        self.pDPhi   = pDPhi
        self.pNSide    = pNSide
        self.pNRZ = pNRZ
        self.pR   = pR
        self.pZ   = pZ
        self.nslice  = nslice

    def pycsgmesh(self):
        polygons = []
        polygonsT = []
        polygonsB = []

        anglerange=self.pDPhi-self.pSPhi
        dPhi  = 2*_np.pi/self.nslice
        stacks  = self.pNRZ
        slices  = self.nslice

        def defineVertex(theta, z, r):
            c = _Vector([0,0,0])
            x = r*_np.cos(theta)
            y = r*_np.sin(theta)

            d = _Vector(
                x,
                y,
                z)
            return d
        def GetAngle(i):
            return self.pSPhi+(anglerange/slices)*(i)

        def appendVertex(vertices, theta, z, r, norm=[]):
            c = _Vector([0,0,0])
            x = r*_np.cos(theta)
            y = r*_np.sin(theta)

            d = _Vector(
                x,
                y,
                z)

            if not norm:
                n = d
            else:
                n = _Vector(norm)
            vertices.append(_Vertex(c.plus(d), None))

		#Bottom
        verticesB = []
        for i in range(self.pNRZ):
            appendVertex(verticesB,self.pSPhi,self.pZ[i],self.pR[i])
        polygons.append(_Polygon(verticesB))

	    #Top
        verticesT = []
        for i in range(self.pNRZ):
            appendVertex(verticesT,self.pDPhi,self.pZ[i],self.pR[i])

		#Midsection
        for l in range(1, slices+1):
            maxn = self.pNRZ
            for n in range(maxn):
                n_up = (n+1)%maxn
                polygons.append(_Polygon([_Vertex(defineVertex(GetAngle(l), self.pZ[n], self.pR[n]), None),
                                          _Vertex(defineVertex(GetAngle(l), self.pZ[n_up], self.pR[n_up]), None),
                                          _Vertex(defineVertex(GetAngle(l-1), self.pZ[n_up], self.pR[n_up]), None),
                                          _Vertex(defineVertex(GetAngle(l-1), self.pZ[n], self.pR[n]), None)]))
        polygons.append(_Polygon(verticesT))
        self.mesh     = _CSG.fromPolygons(polygons)
        return self.mesh


class NullMeshError(Exception):
    """Null Mesh exception.  Should be raised when the output of
    mesh.toPolygons evaluates to the empty list.  arg can be a
    user-provided message (string), or a SolidBase-derived instance."""
    def __init__(self, arg):
        if isinstance(arg, SolidBase):
            self.solid = arg
            if isinstance(self.solid, Intersection):
                self.message = ("Null mesh in intersection between solids:"
                                " {}, {}.".format(self.solid.obj1.name,
                                                  self.solid.obj2.name))
            elif isinstance(self.solid, Subtraction):
                self.message = ("Null mesh in subtraction between solids:"
                                " {}, {}.".format(self.solid.obj1.name,
                                                  self.solid.obj2.name))
            else:
                self.message == "Null mesh in {}.".format(type(self.solid))
            super(Exception, self).__init__(self.message)
        elif isinstance(arg, basestring):
            self.message = arg
            super(Exception, self).__init__(self.message)

def equal_tree(first, second):
    """Check if two boolean solids are identical, not accounting for names"""
    # Both are Boolean instances
    if isinstance(first, Boolean) and isinstance(first, Boolean):
        # Check if they are literally just identical in both sides.
        identical = (equal_tree(first.obj1, second.obj1)
                     and equal_tree(first.obj2, second.obj2)
                     and first.tra2 == second.tra2)
        # Check if they are semantically identical albeit reversed.
        # i.e. union(x,y,tra12) == union(y,x,tra22) iff tra12 ==
        # inverse(tra22)
        rotation = second.tra2[0]
        translation = second.tra2[1]
        inverted_translation = [-1 * x for x in translation]
        inverted_rotation = reverse(rotation)
        inverted_transform = [inverted_rotation, inverted_translation]

        inverted_identical = (equal_tree(first.obj1, second.obj2)
                              and equal_tree(first.obj2, second.obj1)
                              and first.tra2 == inverted_transform)
        # from IPython import embed; embed()
        return identical or inverted_identical

    # Neither are boolean instances, but instead just solids.
    elif isinstance(first, SolidBase) and  isinstance(second, SolidBase):
        return equal_solid(first, second)
    # Not of the same type.
    else:
        return False

def equal_solid(solid1, solid2):
    """Check if two solids are equal, not accounting for their names
    or meshes."""
    if type(solid1) != type(solid2):
        return False
    vars1 = set(vars(solid1))
    vars1.discard("name") # don't check names
    vars1.discard("mesh") # don't check meshes

    for var in vars1:
        if getattr(solid1, var) != getattr(solid2, var):
            return False
    else:
        return True
