import vtk as _vtk

# python iterable to vtkIdList
def mkVtkIdList(it):
    vil = _vtk.vtkIdList()
    for i in it:
        vil.InsertNextId(int(i))
    return vil

class VtkViewer :
    def __init__(self):
        # create a rendering window and renderer
        self.ren = _vtk.vtkRenderer()
        self.renWin = _vtk.vtkRenderWindow()
        self.renWin.AddRenderer(self.ren)

        # create a renderwindowinteractor
        self.iren = _vtk.vtkRenderWindowInteractor()
        self.iren.SetRenderWindow(self.renWin)

        self.ren.SetBackground(1.0, 1.0, 1.0)


        axes = _vtk.vtkAxesActor()
        self.ren.AddActor(axes)

    def addSource(self, meshes):
        print 'VtkViewer.addSource>', meshes
        for m in meshes :
            if type(m) == list :
                self.addSource(m)
            else :
                self.addMesh(m)

    def addMesh(self,m):
        print 'VtkViewer.addMesh>',m
        m.refine()
        verts, cells, count = m.toVerticesAndPolygons()

        meshPD  = _vtk.vtkPolyData()
        points  = _vtk.vtkPoints()
        polys   = _vtk.vtkCellArray()
        scalars = _vtk.vtkFloatArray()

        for v in verts :
            points.InsertNextPoint(v)

        for p in cells :
            polys.InsertNextCell(mkVtkIdList(p))

        for i in range(0,count) :
            scalars.InsertTuple1(i,1)

        meshPD.SetPoints(points)
        meshPD.SetPolys(polys)
        meshPD.GetPointData().SetScalars(scalars)

        del points
        del polys
        del scalars

        triFilter = _vtk.vtkTriangleFilter()

        meshMapper = _vtk.vtkPolyDataMapper()
        if _vtk.VTK_MAJOR_VERSION <= 5:
            triFilter.SetInput(meshPD)
            triFilter.Update()
            meshPD = triFilter.GetOutput()
            meshMapper.SetInput(meshPD)

        else:
            triFilter.SetInputData(meshPD)
            triFilter.Update()
            meshPD = triFilter.GetOutput()
            meshMapper.SetInputData(meshPD)

        meshActor = _vtk.vtkActor()
        try :
            meshActor.GetProperty().SetOpacity(m.alpha);
        except AttributeError :
            pass

        try :
            if m.type == 'logical' :
                meshActor.GetProperty().SetRepresentationToWireframe()
        except AttributeError :
            pass


        meshActor.SetMapper(meshMapper)
        self.ren.AddActor(meshActor)

    def view(self):
        # enable user interface interactor
        self.iren.Initialize()
        self.renWin.Render()
        self.iren.Start()
